import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:refuelio/models/Refill.dart';
import 'package:queries/queries.dart';
import 'package:queries/collections.dart';

class Chart_OctaneEfficiency extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  Chart_OctaneEfficiency(this.seriesList, {this.animate});

  factory Chart_OctaneEfficiency.withData(List<Refill> refills) {
    return new Chart_OctaneEfficiency(
      refills == null ? _createSampleData() : _createRealData(refills),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.LineChart(seriesList,
        defaultRenderer: new charts.LineRendererConfig(includeArea: true, stacked: true),
        behaviors: [new charts.SeriesLegend()],
        animate: animate);
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<Efficiency, int>> _createSampleData() {
    var data = new Map<String,List<Efficiency>>();
    data["98.0"] = [
      new Efficiency(1, 8.2),
      new Efficiency(2, 8.1),
      new Efficiency(3, 8.4),
      new Efficiency(4, 8.3),];
    data["95.0"] = [
      new Efficiency(1, 7.9),
      new Efficiency(2, 7.8),
      new Efficiency(3, 7.9),
      new Efficiency(4, 8.0),];
    data["92.0"] = [
      new Efficiency(1, 8.4),
      new Efficiency(2, 8.6),
      new Efficiency(3, 8.5),
      new Efficiency(4, 8.7),
      new Efficiency(5, 8.6),
      new Efficiency(6, 8.7),];

    return createSeries(data);
  }

  static List<charts.Series<Efficiency, int>> _createRealData(List<Refill> refills) {
    var data = new Map<String,List<Efficiency>>();

    var query = Collection(refills).groupBy((a) => a.octane);

    for (var group in query.asIterable()) {
      var query2 = group.groupBy((a) => a.refilledOn.year);
      for (var group2 in query2.asIterable())
      {
        for(var item in group2.orderBy((a) => a.refilledOn.year).asIterable())
        {
          if(data.containsKey(group.key) == false)
            data[group.key] = new List<Efficiency>();

          data[group.key].add(new Efficiency(data[group.key].length, item.kmPerLitre));
        }
      }
    }
    return createSeries(data);
  }

  static List<charts.Series<Efficiency, int>> createSeries(Map<String,List<Efficiency>> data)
  {
    var series = new List<charts.Series<Efficiency, int>>();
    data.forEach((k,v) =>
    {
      series.add(new charts.Series<Efficiency, int>(
        id: k,
        domainFn: (Efficiency refill, _) => refill.index,
        measureFn: (Efficiency refill, _) => refill.efficiency,
        data: v,
      ))
    });
    return series;
  }
}

/// Sample linear data type.
class Efficiency {
  final int index;
  final double efficiency;

  Efficiency(this.index, this.efficiency);
}
