import 'package:flutter/material.dart';
import 'package:refuelio/models/Refill.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/intl.dart';
import 'package:queries/queries.dart';
import 'package:queries/collections.dart';

class Chart_CostPerLitreOverTime extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  Chart_CostPerLitreOverTime(this.seriesList, {this.animate});

  factory Chart_CostPerLitreOverTime.withData(List<Refill> refills) {
    return new Chart_CostPerLitreOverTime(
      refills == null ? _createSampleData() : _createRealData(refills),
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    final simpleCurrencyFormatter =
    new charts.BasicNumericTickFormatterSpec.fromNumberFormat(
        new NumberFormat.compactSimpleCurrency());
    return new charts.TimeSeriesChart(seriesList,
        animate: animate,
        // Sets up a currency formatter for the measure axis.
        primaryMeasureAxis: new charts.NumericAxisSpec(
            tickFormatterSpec: simpleCurrencyFormatter),

        /// Customizes the date tick formatter. It will print the day of month
        /// as the default format, but include the month and year if it
        /// transitions to a new month.
        ///
        /// minute, hour, day, month, and year are all provided by default and
        /// you can override them following this pattern.
        domainAxis: new charts.DateTimeAxisSpec(
            tickFormatterSpec: new charts.AutoDateTimeTickFormatterSpec(
                day: new charts.TimeFormatterSpec(
                    format: 'd', transitionFormat: 'dd/MM/yyyy'))));
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<FuelOverTime, DateTime>> _createSampleData() {
    var data = [
      new FuelOverTime(new DateTime(2017, 3, 5), 1.45),
      new FuelOverTime(new DateTime(2017, 4, 7), 1.47),
      new FuelOverTime(new DateTime(2017, 4, 9), 1.35),
      new FuelOverTime(new DateTime(2017, 5, 4), 1.32),
      new FuelOverTime(new DateTime(2017, 5, 5), 1.28),
      new FuelOverTime(new DateTime(2017, 6, 7), 1.35),
      new FuelOverTime(new DateTime(2017, 6, 9), 1.45),
      new FuelOverTime(new DateTime(2017, 7, 4), 1.55),
    ];

    return createSeries(data);
  }

  static List<charts.Series<FuelOverTime, DateTime>> _createRealData(List<Refill> refills) {
    var data = new List<FuelOverTime>();

    for(var item in Collection(refills).orderBy((a) => a.refilledOn).asIterable())
    {
      data.add(new FuelOverTime(item.refilledOn, item.cost / item.litres));
    }
    return createSeries(data);
  }

  static List<charts.Series<FuelOverTime, DateTime>> createSeries(List<FuelOverTime> data)
  {
    var series = new List<charts.Series<FuelOverTime, DateTime>>();
    data.forEach((v) =>
    {
      series.add(new charts.Series<FuelOverTime, DateTime>(
        id: 'FuelOverTime',
        domainFn: (FuelOverTime refill, _) => refill.dateTime,
        measureFn: (FuelOverTime refill, _) => refill.costPerLitre,
        data: data,
      ))
    });
    return series;
  }
}

/// Sample data type.
class FuelOverTime {
  final DateTime dateTime;
  final double costPerLitre;

  FuelOverTime(this.dateTime, this.costPerLitre);
}
