import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:refuelio/models/Refill.dart';
import 'package:queries/queries.dart';
import 'package:queries/collections.dart';

class Chart_ServoCostOverTime extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  Chart_ServoCostOverTime(this.seriesList, {this.animate});

  factory Chart_ServoCostOverTime.withData(List<Refill> refills) {
    return new Chart_ServoCostOverTime(
      refills == null ? _createSampleData() : _createRealData(refills),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      seriesList,
      animate: animate,
      barGroupingType: charts.BarGroupingType.grouped,
      behaviors: [new charts.SeriesLegend()],
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<CostOverTime, String>> _createSampleData() {
    var data = new Map<String,List<CostOverTime>>();
    data["Costco"] = [
      new CostOverTime("1989", 5),
      new CostOverTime("1990", 25),
      new CostOverTime("1991", 100),
      new CostOverTime("1992", 75),
      new CostOverTime("1993", 33)];
    data["BP"] = [
      new CostOverTime("1992", 5),
      new CostOverTime("1993", 25),
      new CostOverTime("1994", 100),
      new CostOverTime("1995", 75)
    ];
    data["Caltex"] = [
      new CostOverTime("1994", 5),
      new CostOverTime("1995", 25),
      new CostOverTime("1996", 33)
    ];
    return createSeries(data);
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<CostOverTime, String>> _createRealData(List<Refill> refills)
  {
    var data = new Map<String,List<CostOverTime>>();

    var query = Collection(refills).groupBy((a) => a.servo);

    for (var group in query.asIterable()) {
      var query2 = group.groupBy((a) => a.refilledOn.year);
      for (var group2 in query2.asIterable())
      {
        var average = group2.orderBy((a) => a.refilledOn.year).take(5).average((c) => c.cost);
        if(data.containsKey(group.key) == false)
          data[group.key] = new List<CostOverTime>();

        data[group.key].add(new CostOverTime('${group2.key}', average));
      }
    }
    return createSeries(data);
  }

  static List<charts.Series<CostOverTime, String>> createSeries(Map<String,List<CostOverTime>> data)
  {
    var series = new List<charts.Series<CostOverTime, String>>();
    data.forEach((k,v) =>
    {
      series.add(new charts.Series<CostOverTime, String>(
        id: k,
        domainFn: (CostOverTime refill, _) => refill.year,
        measureFn: (CostOverTime refill, _) => refill.cost,
        data: v,
      ))
    });
    return series;
  }
}

/// Sample linear data type.
class CostOverTime {
  final String year;
  final double cost;
  CostOverTime(this.year, this.cost);
}
