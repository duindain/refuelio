import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:refuelio/models/Refill.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/intl.dart';
import 'package:queries/queries.dart';
import 'package:queries/collections.dart';
import 'package:date_util/date_util.dart';

import '../Utilities.dart';

class Chart_MonthlyCosts extends StatelessWidget {
  final List<charts.Series<MonthlyRefills, String>> seriesList;
  final bool animate;

  Chart_MonthlyCosts(this.seriesList, {this.animate});

  factory Chart_MonthlyCosts.withData(List<Refill> refills) {
    return new Chart_MonthlyCosts(
      refills == null ? _createSampleData() : _createRealData(refills),
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      seriesList,
      animate: animate,
      // Set the default renderer to a bar renderer.
      // This can also be one of the custom renderers of the time series chart.
      barGroupingType: charts.BarGroupingType.grouped,
      // It is recommended that default interactions be turned off if using bar
      // renderer, because the line point highlighter is the default for time
      // series chart.
      behaviors: [
        new charts.PercentInjector(
            totalType: charts.PercentInjectorTotalType.series)
      ],
      // Configure the axis spec to show percentage values.
      primaryMeasureAxis: new charts.PercentAxisSpec(),
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<MonthlyRefills, String>> _createSampleData() {
    var data = [
      new MonthlyRefills("May (19)", 500),
      new MonthlyRefills("Jun (19)", 450),
      new MonthlyRefills("Jul (19)", 375),
      new MonthlyRefills("Aug (19)", 380),
      new MonthlyRefills("Sep (19)", 410),
      new MonthlyRefills("Oct (19)", 200),
      new MonthlyRefills("Nov (19)", 328),
      new MonthlyRefills("Dec (19)", 450),
      new MonthlyRefills("Jan (18)", 500),
      new MonthlyRefills("Feb (18)", 650),
      new MonthlyRefills("Mar (18)", 380),
      new MonthlyRefills("Apr (18)", 576),
    ];

    return createSeries(data);
  }

  static List<charts.Series<MonthlyRefills, String>> _createRealData(List<Refill> refills) {
    var data = new List<MonthlyRefills>();

    var dateUtility = new DateUtil();
    var utilities = GetIt.instance.get<Utilities>();

    var temp = new List<MonthlyData>();

    var query = Collection(refills).groupBy((a) => "$a.refilledOn.month-$a.refilledOn.year");

    for (var group in query.asIterable()) {
      var firstRefill = group.firstOrDefault((a) => a.id > 0);
      if(firstRefill != null)
      {
        temp.add(new MonthlyData(firstRefill.refilledOn.month, firstRefill.refilledOn.year, group.sum$1((b) => b.cost)));
      }
    }

    var sorted = Collection(temp).orderByDescending((a) => a.year).thenByDescending((b) => b.month).take(12);
    for (var item in sorted.asIterable())
    {
      var month = utilities.truncate(3, dateUtility.month(item.month));
      var shortYear = "${item.year}".substring(2);
      data.add(new MonthlyRefills("${month} (${shortYear})", item.cost));
    }
    return createSeries(data);
  }

  static List<charts.Series<MonthlyRefills, String>> createSeries(List<MonthlyRefills> data)
  {
    var series = new List<charts.Series<MonthlyRefills, String>>();
    data.forEach((v) =>
    {
      series.add(new charts.Series<MonthlyRefills, String>(
        id: 'FuelOverTime',
        domainFn: (MonthlyRefills refill, _) => refill.month,
        measureFn: (MonthlyRefills refill, _) => refill.costInMonth,
        data: data,
      ))
    });
    return series;
  }
}

class MonthlyRefills {
  final String month;
  final double costInMonth;

  MonthlyRefills(this.month, this.costInMonth);
}

//Temporary model to store data needed to sort and order data into the correct format
class MonthlyData {
  final int month;
  final int year;
  final double cost;

  MonthlyData(this.month, this.year, this.cost);
}
