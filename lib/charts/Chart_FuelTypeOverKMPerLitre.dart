import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:get_it/get_it.dart';
import 'package:queries/collections.dart';
import 'package:refuelio/models/Refill.dart';
import '../Utilities.dart';

/// Bar chart with series legend example


class Chart_FuelTypeOverKMPerLitre extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  Chart_FuelTypeOverKMPerLitre(this.seriesList, {this.animate});

  factory Chart_FuelTypeOverKMPerLitre.withData(List<Refill> refills) {
    return new Chart_FuelTypeOverKMPerLitre(
      refills == null ? _createSampleData() : _createRealData(refills),
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.PieChart(seriesList,
        animate: animate,
        defaultRenderer: new charts.ArcRendererConfig(arcRendererDecorators: [
          new charts.ArcLabelDecorator(
              labelPosition: charts.ArcLabelPosition.outside)
        ]),
        behaviors: [new charts.DatumLegend()],);
  }

  /// Create series list with multiple series
  static List<charts.Series<AverageKmPerYear, int>> _createSampleData() {
    final sampleData = [
      new AverageKmPerYear(2014, 5.6),
      new AverageKmPerYear(2015, 6.2),
      new AverageKmPerYear(2016, 4.8),
      new AverageKmPerYear(2017, 7.2),
    ];

    return createSeries(sampleData);
  }

  static List<charts.Series<AverageKmPerYear, int>> _createRealData(List<Refill> refills) {
    var data = new List<AverageKmPerYear>();
    var query = Collection(refills).groupBy((a) => a.refilledOn.year);

    for (var group in query.asIterable()) {
      var average = group.average((a) => a.kmPerLitre);
      data.add(new AverageKmPerYear(group.key, average));
    }
    return createSeries(data);
  }

  static List<charts.Series<AverageKmPerYear, int>> createSeries(List<AverageKmPerYear> data)
  {
    var utilities = GetIt.instance.get<Utilities>();
    return [
      new charts.Series<AverageKmPerYear, int>(
        id: 'AverageKmPerYear',
        domainFn: (AverageKmPerYear avgKmPerLitre, _) => avgKmPerLitre.year,
        measureFn: (AverageKmPerYear avgKmPerLitre, _) => avgKmPerLitre.kmPerLitre,
        data: data,
        labelAccessorFn: (AverageKmPerYear row, _) => ' ${row.kmPerLitre} ${utilities.getPreferenceAsBool("Kilometers") ? 'Km/L' : 'M/G'}',
      )
    ];
  }
}

/// Sample ordinal data type.
class AverageKmPerYear {
  final int year;
  final double kmPerLitre;

  AverageKmPerYear(this.year, this.kmPerLitre);
}