import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:get_it/get_it.dart';
import 'package:queries/collections.dart';
import 'package:refuelio/models/Refill.dart';

import '../Utilities.dart';

class Chart_AllRefillsCostsOverTime extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  Chart_AllRefillsCostsOverTime(this.seriesList, {this.animate});

  factory Chart_AllRefillsCostsOverTime.withData(List<Refill> refills) {
    return new Chart_AllRefillsCostsOverTime(
      refills == null ? _createSampleData() : _createRealData(refills),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.ScatterPlotChart(seriesList, animate: animate);
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<RefillRecord, int>> _createSampleData() {
    var data = new Map<String,List<RefillRecord>>();
    data["refills"] = [
      new RefillRecord(05, 45, 4.5),
      new RefillRecord(06, 55, 5.5),
      new RefillRecord(07, 44, 4.4),
      new RefillRecord(08, 47, 4.7),
      new RefillRecord(09, 65, 6.5),
      new RefillRecord(10, 63, 6.3),
      new RefillRecord(11, 75, 7.5),
      new RefillRecord(12, 22, 2.2),
      new RefillRecord(13, 15, 1.5),
      new RefillRecord(14, 42, 4.2),
      new RefillRecord(15, 85, 8.5),
      new RefillRecord(16, 47, 4.7),
      new RefillRecord(17, 55, 5.5),
      new RefillRecord(18, 48, 4.8),
    ];
/*
    data["average"] = [
      new RefillRecord(0, 5, 3.5),
      new RefillRecord(56, 240, 3.5),
    ];*/
    return createSeries(data);
  }

  static List<charts.Series<RefillRecord, int>> _createRealData(List<Refill> refills) {
    var utilities = GetIt.instance.get<Utilities>();
    var data = new Map<String,List<RefillRecord>>();
    var query = Collection(refills).groupBy((a) => a.refilledOn.year);

    data["refills"] = new List<RefillRecord>();
    for (var group in query.asIterable()) {
      var average = group.average((a) => a.cost);
      data["refills"].add(new RefillRecord(int.tryParse(utilities.truncate(2, "${group.key}")), average, average / 10));
    }
    return createSeries(data);
  }

  static List<charts.Series<RefillRecord, int>> createSeries(Map<String, List<RefillRecord>> data)
  {
    var maxMeasure = Collection(data["refills"]).max$1((a) => a.totalCost);
    return [
      new charts.Series<RefillRecord, int>(
        id: 'Sales',
        // Providing a color function is optional.
        colorFn: (RefillRecord refill, _) {
          // Bucket the measure column value into 3 distinct colors.
          final bucket = refill.totalCost / maxMeasure;

          if (bucket < 1 / 3) {
            return charts.MaterialPalette.blue.shadeDefault;
          } else if (bucket < 2 / 3) {
            return charts.MaterialPalette.red.shadeDefault;
          } else {
            return charts.MaterialPalette.green.shadeDefault;
          }
        },
        domainFn: (RefillRecord refill, _) => refill.year,
        measureFn: (RefillRecord refill, _) => refill.totalCost,
        radiusPxFn: (RefillRecord refill, _) => refill.radius,
        data: data["refills"],
      )
    ];
  }
}

/// Sample linear data type.
class RefillRecord {
  final int year;
  final double totalCost;
  final double radius;

  RefillRecord(this.year, this.totalCost, this.radius);
}
