import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:refuelio/Utilities.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:mime/mime.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'RefillForm.dart';
import 'SlideRightRoute.dart';
import 'models/Refill.dart';

class GetScan extends StatefulWidget {
  GetScan() : super();

  @override
  _GetScanState createState() => _GetScanState();
}

class _GetScanState extends State<GetScan>
{
  var utilities = GetIt.instance.get<Utilities>();
  File _imageFile;
  dynamic _pickImageError;
  String _retrieveDataError;

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        title: Text("Scan or fetch image of Receipt"),
      ),
      body: Padding(
          padding: EdgeInsets.only(top:20, left: 10.0, right: 10.0),
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children:[
      Center(
        child: Platform.isIOS ? _previewImage(context) :
        FutureBuilder<void>(
          future: retrieveLostData(),
          builder: (BuildContext context, AsyncSnapshot<void> snapshot)
          {
            switch (snapshot.connectionState)
            {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return const Text(
                  'Please select from your gallery or take a photo of a receipt.',
                  textAlign: TextAlign.center,
                );
              case ConnectionState.done:
                return _previewImage(context);
              default:
                if (snapshot.hasError)
                {
                  return Text(
                    'Pick image/video error: ${snapshot.error}}',
                    textAlign: TextAlign.center,
                  );
                }
                else
                {
                  return const Text(
                    'Please select from your gallery or take a photo of a receipt.',
                    textAlign: TextAlign.center,
                  );
                }
            }
          },
        ),
      ),
      Expanded(
        child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children:[
        Align(
          alignment: Alignment.bottomLeft,
          child: utilities.fancyPantsButton(context, Icons.photo_library, "Select from gallery", "Pick Image from gallery", () =>
          {
            _onImageButtonPressed(ImageSource.gallery)
          })
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: utilities.fancyPantsButton(context, Icons.camera_alt, "Take a Photo", "Take a Photo", () =>
          {
            _onImageButtonPressed(ImageSource.camera)
          })
        ),]
      )
    )]
      )
      ),
    );
  }

  Text _getRetrieveErrorWidget()
  {
    if (_retrieveDataError != null)
    {
      final Text result = Text(_retrieveDataError);
      _retrieveDataError = null;
      return result;
    }
    return null;
  }

  Future<void> retrieveLostData()
  async {
    final LostDataResponse response = await ImagePicker.retrieveLostData();
    if (response.isEmpty)
    {
      return;
    }
    if (response.file != null)
    {
      setState(()
      {
        _imageFile = response.file;
      });
    }
    else
    {
      _retrieveDataError = response.exception.code;
    }
  }

  Widget _previewImage(BuildContext context)
  {
    final Text retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null)
    {
      return retrieveError;
    }
    if (_imageFile != null)
    {
      return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children:[
            Image.file(_imageFile),
            Padding(
              padding: EdgeInsets.only(top:20),
              child:utilities.fancyPantsButton(context, Icons.send, "Process image", "Process image for refill data", () =>
              {
                _processImage(context, _imageFile)
              })
            )

      ]);
    }
    else if (_pickImageError != null)
    {
      return Text(
        'Pick image error: $_pickImageError',
        textAlign: TextAlign.center,
      );
    }
    else
    {
      return const Text(
        'You have not yet picked an image.',
        textAlign: TextAlign.center,
      );
    }
  }
  void _onImageButtonPressed(ImageSource source) async {
    try {
        _imageFile = await ImagePicker.pickImage(source: source);
        setState(() {});
      } catch (e) {
        _pickImageError = e;
      }
    }

  Future _processImage(BuildContext context, File imageFile) async
  {
    var mimeType = lookupMimeType(extension(imageFile.path));
    var response = await http.post(
        Uri.parse(utilities.baseUrl),
      body: {
        "ocr" : "true",
        "photo" : 'data:' + mimeType + ';base64,' + convert.base64Encode(imageFile.readAsBytesSync())
      }
    );
    if (response.statusCode == 200)
    {
      String responseText = response.body.toString();
      print("responseText $responseText");
      if(utilities.isJson(responseText))
      {
        var jsonResponse = convert.jsonDecode(responseText);
        print("downloaded $jsonResponse");
      }
      else
      {
        utilities.createAlert(context, AlertType.error, "Scan failed", responseText);
      }




/*
    var file = await http.MultipartFile.fromBytes("ocrScanImageFile", await imageFile.readAsBytes());
    var request = http.MultipartRequest('POST', Uri.parse(utilities.baseUrl+"?ocr=true"));
    request.files.add(file);
    var response = await request.send();
    if (response.statusCode == 200)
    {
      var jsonResponse = convert.jsonDecode(response..body);
*/
      //Should return a json key value list which we need to parse into a refill then pop off the last few screens and reopen the refill with the parsed data

      /*
      Should we display a popup with imported data?
      //Close Image Scan page
      Navigator.pop(context);
      //Close RefillForm page
      Navigator.pop(context);
      var refill = Refill.fromJson(jsonResponse);
      Navigator.push(context, SlideRightRoute(page: RefillForm(refill), routeName: "RefillForm"));
      */

    }
    print("_processImage response ${response.statusCode}");
  }
}