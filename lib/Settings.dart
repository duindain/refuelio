import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:refuelio/EditVehicles.dart';
import 'SlideRightRoute.dart';
import 'Utilities.dart';
import 'models/DatabaseService.dart';
import 'models/Vehicle.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Settings extends StatefulWidget
{
  @override
  _SettingsFormState createState() => _SettingsFormState();
}

class _SettingsFormState extends State<Settings> {

  var utilities = GetIt.instance.get<Utilities>();
  var db = GetIt.instance.get<DatabaseService>();
  List<Vehicle> vehicles;
  bool loading = false;
  Vehicle vehicle = new Vehicle();
  TextEditingController servoController;
  TextEditingController octaneController;

  @override
  void initState() {
    _asyncMethod();
    super.initState();
  }

  _asyncMethod() async {
    db.stream$.listen((x) async {
      loading = true;
      print("Settings: Vehicles stream update detected");
      vehicles = await db.vehicles();
      vehicles.sort((a, b) => a.name.compareTo(b.name));
      loading = false;
      print("Settings: Vehicles updated");
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    if(loading)
    {
      return SpinKitWave(color: Colors.orange, type: SpinKitWaveType.center);
    }

    servoController = TextEditingController(text:utilities.getPreferenceAsString("ServoName", emptyValue: "local"));
    octaneController = TextEditingController(text:utilities.getPreferenceAsString("Octane", emptyValue: "'98.0"));

    var hasVehicles = vehicles != null && vehicles.any((a) => a.id > 0);

    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Settings")
        ),
        body:
    SingleChildScrollView(
    scrollDirection: Axis.vertical,
    child:new Padding(
          padding:new EdgeInsets.all(16.0),
          child:
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text("Multiple vehicles enabled"),
                Switch(
                    value: utilities.getPreferenceAsBool("MultipleVehiclesEnabled", emptyValue: true),
                    onChanged: (value) {
                      utilities.sharedPreferences.setBool("MultipleVehiclesEnabled", value);
                      setState(() {});
                    }
                )
              ],
            ),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[Expanded(
                  child: Divider()
              )],
            ),
            Visibility (
                visible: true,//utilities.getPreferenceAsBool("OCREnabled") ?? false,
                child: RaisedButton(
                    onPressed: () {
                      Navigator.push(context, SlideRightRoute(page: EditVehicles(), routeName: "Change Vehicles"));
                    },
                    child:Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[ Icon(
                          Icons.directions_car,
                          color: Colors.white70,
                          size: 24.0,
                          semanticLabel: 'Delete, edit or add vehicles',
                        ),Text(' Vehicles')])
                )
            ),

            if(hasVehicles && utilities.getPreferenceAsBool("MultipleVehiclesEnabled") ?? false)
            Row(crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[Expanded(
                  child: Divider()
              )],
            ),
            if(hasVehicles && utilities.getPreferenceAsBool("MultipleVehiclesEnabled") ?? false)
            Row(crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Select your default vehicle")
                ]
            ),
            if(hasVehicles && utilities.getPreferenceAsBool("MultipleVehiclesEnabled") ?? false)
              new DropdownButton(
                  hint: Text('Default vehicle'),
                  items: vehicles.map((Vehicle vehicle) {
                    return new DropdownMenuItem(
                      value: vehicle,
                      child: new Text(vehicle.name),
                    );
                  }).toList(),
                  onChanged: (newValue)
                  {
                    utilities.sharedPreferences.setInt("CurrentVehicle", newValue.id);
                    setState(() {});
                  },
                  value: db.getCurrentVehicle(vehicles)),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[Expanded(
                  child: Divider()
              )],
            ),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text("Theme Dark or Light"),
                Switch(
                    value: utilities.getPreferenceAsBool("DarkTheme", emptyValue: true),
                    onChanged: (value) {
                      print("Dark theme Selected "+value.toString());
                      utilities.sharedPreferences.setBool("DarkTheme", value);
                    }
                )
              ],
            ),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Theme change will activate next time the app loads."),]),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[Expanded(
                  child: Divider()
              )],
            ),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text("Graphs enabled"),
                Switch(
                    value: utilities.getPreferenceAsBool("GraphsEnabled", emptyValue: true),
                    onChanged: (value) {
                      print("Graph display Selected "+value.toString());
                      utilities.sharedPreferences.setBool("GraphsEnabled", value);
                    }
                )
              ],
            ),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[Expanded(
                child: Divider()
              )],
            ),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                Text("Miles or Kilometers"),
                Switch(
                  value: utilities.getPreferenceAsBool("Kilometers", emptyValue: true),
                  onChanged: (value) {
                    print("Miles or KM Selected "+value.toString());
                    utilities.sharedPreferences.setBool("Kilometers", value);
                  }),
            ]),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[Expanded(
                  child: Divider()
              )],
            ),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Gallons or Litres"),
                  Switch(
                      value: utilities.getPreferenceAsBool("Litres", emptyValue: true),
                      onChanged: (value) {
                        print("Gallons or Litres Selected "+value.toString());
                        utilities.sharedPreferences.setBool("Litres", value);
                      }),
                ]),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[Expanded(
                  child: Divider()
              )],
            ),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text("Default Servo name")
              ]
            ),

            Row(crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                Expanded(
                  child:TextField(
                  textAlign: TextAlign.center,
                  controller: servoController,
                  onChanged: (text) =>
                  {
                    //servoController.text = v,
                    utilities.sharedPreferences.setString("ServoName", text)
                  })
                ),
              ]),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[Expanded(
                  child: Divider()
              )],
            ),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Default Octane value")
                ]
            ),
            Row(crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child:TextField(
                          textAlign: TextAlign.center,
                          controller: octaneController,
                          onChanged: (text) =>
                          {
                            //servoController.text = v,
                            utilities.sharedPreferences.setString("Octane", text)
                          })
                  ),
                ]),
            ],
        )
      )
    )
    );
  }

  @override
  void dispose() {
    servoController?.dispose();
    octaneController?.dispose();
    super.dispose();
  }
}