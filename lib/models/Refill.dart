import 'dart:convert';

Refill refillFromJson(String str) => Refill.fromJson(json.decode(str));

String refillToJson(Refill data) => json.encode(data.toJson());

class Refill {
  int id;
  int vehicleId;
  String servo;
  double litres;
  double kmPerLitre;
  String octane;
  double cost;
  DateTime createdOn;
  DateTime refilledOn;

  Refill({
    this.id,
    this.vehicleId,
    this.servo,
    this.litres,
    this.kmPerLitre,
    this.octane,
    this.cost,
    this.createdOn,
    this.refilledOn,
  });

  factory Refill.fromJson(Map<String, dynamic> json) => Refill(
    id: json["id"],
    vehicleId: json['vehicleId'],
    servo: json["servo"],
    litres: json["litres"].toDouble(),
    kmPerLitre: json["KmPerLitre"].toDouble(),
    octane: json["octane"],
    cost: json["cost"].toDouble(),
    createdOn: DateTime.parse(json["createdOn"]),
    refilledOn: DateTime.parse(json["refilledOn"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "vehicleId": vehicleId,
    "servo": servo,
    "litres": litres,
    "KmPerLitre": kmPerLitre,
    "octane": octane,
    "cost": cost,
    "createdOn": "${createdOn.year.toString().padLeft(4, '0')}-${createdOn.month.toString().padLeft(2, '0')}-${createdOn.day.toString().padLeft(2, '0')}",
    "refilledOn": "${refilledOn.year.toString().padLeft(4, '0')}-${refilledOn.month.toString().padLeft(2, '0')}-${refilledOn.day.toString().padLeft(2, '0')}",
  };
}