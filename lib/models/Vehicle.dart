import 'dart:convert';

Vehicle VehicleFromJson(String str) => Vehicle.fromJson(json.decode(str));

String VehicleToJson(Vehicle data) => json.encode(data.toJson());

class Vehicle {
  int id;
  String name;
  DateTime createdOn;

  Vehicle({
           this.id,
           this.name,
           this.createdOn,
         });

  factory Vehicle.fromJson(Map<String, dynamic> json) => Vehicle(
    id: json["id"],
    name: json["name"],
    createdOn: DateTime.parse(json["createdOn"])
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "createdOn": "${createdOn.year.toString().padLeft(4, '0')}-${createdOn.month.toString().padLeft(2, '0')}-${createdOn.day.toString().padLeft(2, '0')}",
  };
}