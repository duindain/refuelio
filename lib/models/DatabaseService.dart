import 'dart:async';
import 'package:refuelio/Utilities.dart';
import 'package:path/path.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sqflite/sqflite.dart';
import 'Refill.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:intl/intl.dart';
import 'Vehicle.dart';

class DatabaseService
{
  DateFormat dbDateTimeFormat = DateFormat("EEE, MMM d, yyyy HH:mm aaa"); //database datetime format
  Future<Database> database;

  BehaviorSubject _dbItemsCount = BehaviorSubject.seeded(0);
  Observable get stream$ => _dbItemsCount.stream;
  var utilities = GetIt.instance.get<Utilities>();
  //int get current => _dbItemsCount.value;

  Future<Database> initilise() async {
      const initScript = [
        '''CREATE TABLE if not exists 
            vehicles(
              id INTEGER PRIMARY KEY autoincrement, 
              name TEXT, 
              createdOn TEXT)''',
        '''CREATE TABLE if not exists 
            refills(
              id INTEGER PRIMARY KEY autoincrement, 
              vehicleId INTEGER not null, 
              servo TEXT, 
              litres REAL, 
              KmPerLitre REAL, 
              octane TEXT, 
              cost REAL, 
              createdOn TEXT, 
              refilledOn TEXT)''',
        ];
      const migrationScripts = [];
      database = openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), 'refuelio.db'),
        // When the database is first created, create a table to store Refills and Vehicles.
        onCreate: (db, version)
        {
          initScript.forEach((script) async => await db.execute(script));
        },
        onUpgrade: (Database db, int oldVersion, int newVersion) async
        {
          for (var i = oldVersion - 1; i <= newVersion - 1; i++)
          {
            await db.execute(migrationScripts[i]);
          }
        },
        version: migrationScripts.length + 1,
      );
      return database;
    }

    //Refill SQL
  Future<int> insertRefill(Refill refill) async {
    // Get a reference to the database.
    final Database db = await database;

    // Insert the Refill into the correct table. Also specify the
    // `conflictAlgorithm`. In this case, if the same Refill is inserted
    // multiple times, it replaces the previous data.
    var changed = await db.insert(
      'Refills',
      refill.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    if(changed > 0)
    {
      _dbItemsCount.value = _dbItemsCount.value + changed;

      if(utilities.getPreferenceAsBool("OffsiteBackup"))
      {
        var response = await http.post(utilities.baseUrl, body: {'clientId' : utilities.getPreferenceAsString("ClientId"), 'method' : 'insert', 'table': 'refills', 'data': refill.toJson()} );
        print(response.statusCode);
      }
    }
    return changed;
  }

  Future<List<Refill>> refills({bool currentVehicle = true}) async {
    if(utilities.getPreferenceAsBool("OffsiteBackup"))
    {
      var response = await http.post(utilities.baseUrl, body: {'clientId' : utilities.getPreferenceAsString("ClientId"), 'method' : 'getall', 'table': 'refills', 'data' : "true"} );
      if (response.statusCode == 200)
      {
        var jsonResponse = convert.jsonDecode(response.body);
        var items =  List.generate(jsonResponse.length, (i) {
          return Refill.fromJson(jsonResponse[i]);
        });
        print("downloaded "+items.length.toString()+" items");
      }
    }
    // Get a reference to the database.
    final Database db = await database;
    var currentVehicleId = utilities.getPreferenceAsInt("CurrentVehicle");
    // Query the table for all The Refills.
    final List<Map<String, dynamic>> maps = currentVehicle && currentVehicleId > 0 ?
      await db.query('Refills', where:'vehicleId=?', whereArgs: [currentVehicleId]) :
      await db.query('Refills');

    // Convert the List<Map<String, dynamic> into a List<Refill>.
    return List.generate(maps.length, (i) {
      return Refill.fromJson(maps[i]);
    });
  }

  Future<int> updateRefill(Refill refill) async {
    // Get a reference to the database.
    final db = await database;

    // Update the given Refill.
    var changed = await db.update(
      'Refills',
      refill.toJson(),
      // Ensure that the Refill has a matching id.
      where: "id = ?",
      // Pass the Refill's id as a whereArg to prevent SQL injection.
      whereArgs: [refill.id],
    );
    if(changed > 0)
    {
      _dbItemsCount.value = _dbItemsCount.value + changed;
      if(utilities.getPreferenceAsBool("OffsiteBackup"))
      {
        var response = await http.post(utilities.baseUrl, body: {'clientId' : utilities.getPreferenceAsString("ClientId"), 'method': 'update', 'table': 'refills', 'data' : refill.toJson()} );
        print(response.statusCode);
      }
    }
    return changed;
  }

  Future<int> deleteRefill(int id) async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Refill from the database.
    var changed = await db.delete(
      'Refills',
      // Use a `where` clause to delete a specific Refill.
      where: "id = ?",
      // Pass the Refill's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
    if(changed > 0)
    {
      _dbItemsCount.value = _dbItemsCount.value + changed;
      if(utilities.getPreferenceAsBool("OffsiteBackup"))
      {
        var response = await http.post(utilities.baseUrl, body: {'clientId' : utilities.getPreferenceAsString("ClientId"), 'method' : 'delete', 'table': 'refills', 'data' : id} );
        print(response.statusCode);
      }
    }
    return changed;
  }

  //Vehicle SQL
  Future<int> insertVehicle(Vehicle vehicle) async {
    // Get a reference to the database.
    final Database db = await database;

    // Insert the Vehicle into the correct table. Also specify the
    // `conflictAlgorithm`. In this case, if the same Refill is inserted
    // multiple times, it replaces the previous data.
    var changed = await db.insert(
      'Vehicles',
      vehicle.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    if(changed > 0)
    {
      _dbItemsCount.value = _dbItemsCount.value + changed;

      if(utilities.getPreferenceAsBool("OffsiteBackup"))
      {
        var response = await http.post(utilities.baseUrl, body: {'clientId' : utilities.getPreferenceAsString("ClientId"), 'method' : 'insert', 'table': 'vehicles', 'data': vehicle.toJson()} );
        print(response.statusCode);
      }
    }
    return changed;
  }

  Future<List<Vehicle>> vehicles() async {
    if(utilities.getPreferenceAsBool("OffsiteBackup"))
    {
      var response = await http.post(utilities.baseUrl, body: {'clientId' : utilities.getPreferenceAsString("ClientId"), 'method' : 'getall', 'table': 'vehicles', 'data' : "true"} );
      if (response.statusCode == 200)
      {
        var jsonResponse = convert.jsonDecode(response.body);
        var items =  List.generate(jsonResponse.length, (i) {
          return Vehicle.fromJson(jsonResponse[i]);
        });
        print("downloaded "+items.length.toString()+" items");
      }
    }
    // Get a reference to the database.
    final Database db = await database;

    // Query the table for all The Refills.
    final List<Map<String, dynamic>> maps = await db.query('Vehicles');

    // Convert the List<Map<String, dynamic> into a List<Refill>.
    return List.generate(maps.length, (i) {
      return Vehicle.fromJson(maps[i]);
    });
  }

  Future<int> updateVehicle(Vehicle vehicle) async {
    // Get a reference to the database.
    final db = await database;

    // Update the given Refill.
    var changed = await db.update(
      'Vehicles',
      vehicle.toJson(),
      // Ensure that the Vehicle has a matching id.
      where: "id = ?",
      // Pass the Vehicle's id as a whereArg to prevent SQL injection.
      whereArgs: [vehicle.id],
    );
    if(changed > 0)
    {
      _dbItemsCount.value = _dbItemsCount.value + changed;
      if(utilities.getPreferenceAsBool("OffsiteBackup"))
      {
        var response = await http.post(utilities.baseUrl, body: {'clientId' : utilities.getPreferenceAsString("ClientId"), 'method': 'update', 'table': 'vehicles', 'data' : vehicle.toJson()} );
        print(response.statusCode);
      }
    }
    return changed;
  }

  Future<int> deleteVehicle(int id) async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Refill from the database.
    var changed = await db.delete(
      'Vehicles',
      // Use a `where` clause to delete a specific Vehicle.
      where: "id = ?",
      // Pass the Vehicle's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
    if(changed > 0)
    {
      _dbItemsCount.value = _dbItemsCount.value + changed;
      if(utilities.getPreferenceAsBool("OffsiteBackup"))
      {
        var response = await http.post(utilities.baseUrl, body: {'clientId' : utilities.getPreferenceAsString("ClientId"), 'method' : 'delete', 'table': 'vehicles', 'data' : id} );
        print(response.statusCode);
      }
    }
    return changed;
  }


  Vehicle getCurrentVehicle(List<Vehicle> vehicles)
  {
    if(vehicles != null && vehicles.length == 1)
      return vehicles.first;
    else if (vehicles != null)
      return vehicles.firstWhere((a) => a.id == utilities.getPreferenceAsInt("CurrentVehicle"), orElse: () => null);
    else
      return null;
  }
}