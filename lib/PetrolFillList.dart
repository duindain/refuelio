import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_it/get_it.dart';
import 'dart:core';
import 'RefillForm.dart';
import 'SlideRightRoute.dart';
import 'models/DatabaseService.dart';
import 'models/Refill.dart';
import 'Utilities.dart';

class PetrolFillList extends StatefulWidget {

  PetrolFillList();

  @override
  PetrolFillListState createState() => new PetrolFillListState();
}

class PetrolFillListState extends State<PetrolFillList> {
  var db = GetIt.instance.get<DatabaseService>();
  var utilities = GetIt.instance.get<Utilities>();
  List<Refill> refills;
  bool sortOrder;
  var loading = false;
  int sortColumn;

  @override
  void initState() {
    _asyncMethod();
    super.initState();
  }

  _asyncMethod() async {
    db.stream$.listen((x) async {
      loading = true;
      print("PetrolFillList: Refills stream update detected");
      sortOrder = false;
      sortColumn = 0;
      refills = await db.refills(currentVehicle: true);
      //refills.sort((a, b) => a.refilledOn.compareTo(b.refilledOn));
      loading = false;
      print("PetrolFillList: Refills updated");
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    if(loading)
      return SpinKitWave(color: Colors.orange, type: SpinKitWaveType.center);

    if((refills == null || refills.length == 0) == false)
  {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: FittedBox(
      child: DataTable(
        sortColumnIndex: sortColumn,
        sortAscending: sortOrder,
        columns: [
          DataColumn(label: new Text('Servo'),
          onSort: (columnIndex, ascending) {
            setState(() {
              sortColumn = columnIndex;
              sortOrder = !sortOrder;
            });
            onSortColumn(refills, columnIndex, ascending);
          }),
          DataColumn(
            label: new Text(utilities.getPreferenceAsBool("Litres") ? 'Litres' : 'Gallons'),
            numeric:true,
            onSort: (columnIndex, ascending) {
              setState(() {
                sortColumn = columnIndex;
                sortOrder = !sortOrder;
              });
              onSortColumn(refills, columnIndex, ascending);
          }),
          DataColumn(label: new Text(utilities.getPreferenceAsBool("Kilometers") ? 'Km/L' : 'M/G'), numeric:true,
            onSort: (columnIndex, ascending) {
              setState(() {
                sortColumn = columnIndex;
                sortOrder = !sortOrder;
            });
            onSortColumn(refills, columnIndex, ascending);
          }),
          DataColumn(label: new Text('Octane'),
              onSort: (columnIndex, ascending) {
                setState(() {
                  sortColumn = columnIndex;
                  sortOrder = !sortOrder;
                });
                onSortColumn(refills, columnIndex, ascending);
              }),
          DataColumn(label: new Text('Cost'), numeric:true,
              onSort: (columnIndex, ascending) {
                setState(() {
                  sortColumn = columnIndex;
                  sortOrder = !sortOrder;
                });
                onSortColumn(refills, columnIndex, ascending);
              }),
        ],
        rows:
        refills.map(
          ((refill) =>
              DataRow(
                cells: <DataCell>[
                  DataCell(Text(refill.servo),
                    onTap: () { Navigator.push(context, SlideRightRoute(page: RefillForm(refill), routeName: "RefillForm")); }),
                  DataCell(Text(refill.litres.toString()),
                    onTap: () { Navigator.push(context, SlideRightRoute(page: RefillForm(refill), routeName: "RefillForm")); }),
                  DataCell(Text(refill.kmPerLitre.toString()),
                    onTap: () { Navigator.push(context, SlideRightRoute(page: RefillForm(refill), routeName: "RefillForm")); }),
                  DataCell(Text(refill.octane),
                    onTap: () { Navigator.push(context, SlideRightRoute(page: RefillForm(refill), routeName: "RefillForm")); }),
                  DataCell(Text(refill.cost.toString()),
                    onTap: () { Navigator.push(context, SlideRightRoute(page: RefillForm(refill), routeName: "RefillForm")); }),
                ],
              )),
        ).toList(),
      )));
    }
    else
    {
      return Text("Please add some refills to have data appear in a table here.");
    }
  }
  onSortColumn(List<Refill> refills, int columnIndex, bool ascending) {
    switch(columnIndex)
    {
      case 0:
        refills.sort((a, b) => ascending ? a.servo.compareTo(b.servo) : b.servo.compareTo(a.servo));
        break;
      case 1:
        refills.sort((a, b) => ascending ? a.litres.compareTo(b.litres) : b.litres.compareTo(a.litres));
        break;
      case 2:
        refills.sort((a, b) => ascending ? a.kmPerLitre.compareTo(b.kmPerLitre) : b.kmPerLitre.compareTo(a.kmPerLitre));
        break;
      case 3:
        refills.sort((a, b) => ascending ? a.octane.compareTo(b.octane) : b.octane.compareTo(a.octane));
        break;
      case 4:
        refills.sort((a, b) => ascending ? a.cost.compareTo(b.cost) : b.cost.compareTo(a.cost));
        break;
      default:
        print("Unknown column index in sort "+columnIndex.toString());
        break;
    }
  }
}