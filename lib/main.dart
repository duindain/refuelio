import 'dart:async';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_crashlytics/flutter_crashlytics.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:refuelio/Utilities.dart';
import 'package:refuelio/theme_blue.dart';
import 'package:refuelio/theme_dark.dart';
import 'package:get_it/get_it.dart';
import 'CarouselWithIndicator.dart';
import 'PetrolFillList.dart';
import 'RefillForm.dart';
import 'Settings.dart';
import 'SlideRightRoute.dart';
import 'models/DatabaseService.dart';
import 'models/Refill.dart';
import 'models/Vehicle.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  FirebaseAnalytics analytics = FirebaseAnalytics();
  GetIt.instance.registerSingleton<Utilities>(Utilities());
  GetIt.instance.registerSingleton<DatabaseService>(DatabaseService());

  var utilities = GetIt.instance.get<Utilities>();
  await utilities.initilise();

  var db = GetIt.instance.get<DatabaseService>();
  await db.initilise();

  bool isInDebugMode = false;

  FlutterError.onError = (FlutterErrorDetails details) {
    if (isInDebugMode) {
      FlutterError.dumpErrorToConsole(details);
    } else {
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  await FlutterCrashlytics().initialize();

  runZoned<Future<Null>>(() async {
    runApp(new MaterialApp(
      theme: utilities.getPreferenceAsBool("DarkTheme") ? theme_dark : theme_blue,
      home: new RefuelioApp(),
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
    ));
  }, onError: (error, stackTrace) async {
    // Whenever an error occurs, call the `reportCrash` function. This will send
    // Dart errors to our dev console or Crashlytics depending on the environment.
    await FlutterCrashlytics().reportCrash(error, stackTrace, forceCrash: false);
  });
}

class RefuelioApp extends StatefulWidget {
  @override
  _RefuelioAppState createState() => new _RefuelioAppState();
}

class _RefuelioAppState extends State<RefuelioApp> {

  RateMyApp rateMyApp = RateMyApp(
    preferencesPrefix: 'rateMyApp_',
    minDays: 7,
    minLaunches: 10,
    remindDays: 7,
    remindLaunches: 10,
    googlePlayIdentifier: 'com.lazinator.refuelio',
    appStoreIdentifier: '4974173751655094822',
  );

  var db = GetIt.instance.get<DatabaseService>();
  var utilities = GetIt.instance.get<Utilities>();
  List<Vehicle> vehicles;
  bool loading = false;
  bool hasVehicles = false;

  @override
  void initState() {
    _asyncMethod();
    super.initState();
  }

  _asyncMethod() async {
    db.stream$.listen((x) async {
      loading = true;
      print("RefuelioApp: Vehicles stream update detected");
      vehicles = await db.vehicles();
      vehicles.sort((a, b) => a.name.compareTo(b.name));
      hasVehicles = vehicles != null && vehicles.any((a) => a.id > 0);
      loading = false;
      print("RefuelioApp: Vehicles updated");
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context)
  {
    if(loading)
    {
      return SpinKitWave(color: Colors.orange, type: SpinKitWaveType.center);
    }
    rateMyApp.init().then((_)
    {
      rateMyApp.conditions.forEach((condition)
      {
        if (condition is DebuggableCondition)
        {
          print(condition.valuesAsString()); // We iterate through our list of conditions and we print all debuggable ones.
        }
      });

      if (rateMyApp.shouldOpenDialog)
        rateMyApp.showRateDialog(
          context,
          title: 'Rate this app',
          // The dialog title.
          message: 'If you like this app, please take a little bit of your time to review it!\nIt really helps us and it shouldn\'t take you more than a minute.',
          // The dialog message.
          rateButton: 'RATE',
          // The dialog "rate" button text.
          noButton: 'NO THANKS',
          // The dialog "no" button text.
          laterButton: 'MAYBE LATER',
          // The dialog "later" button text.
          listener: (button)
          { // The button click listener (useful if you want to cancel the click event).
            switch (button)
            {
              case RateMyAppDialogButton.rate:
                Navigator.pop(context);
                return true;
                break;
              case RateMyAppDialogButton.later:
                rateMyApp.callEvent(RateMyAppEventType.laterButtonPressed);
                Navigator.pop(context);
                break;
              case RateMyAppDialogButton.no:
                showDialog(
                  context: context,
                  builder: (BuildContext context)
                  {
                    // return object of type Dialog
                    return AlertDialog(
                      title: new Text("Can we improve?"),
                      content: new Text("Would you like to give some suggestions for improvements we could make to make the app more suitable, you can email details by clicking the Email us button."),
                      actions: <Widget>[
                        // usually buttons at the bottom of the dialog
                        new FlatButton(
                          child: new Text("Email us"),
                          onPressed: ()
                          {
                            var email = Email(
                              body: '',
                              subject: 'Refuelio app',
                              recipients: ['lazinatorapps@gmail.com'],
                              isHTML: false,
                            );
                            FlutterEmailSender.send(email);
                            rateMyApp.callEvent(RateMyAppEventType.laterButtonPressed).then((_) =>
                            {
                              Navigator.pop(context),
                              Navigator.pop(context)
                            });
                          },
                        ),
                        new FlatButton(
                          child: new Text("No thanks"),
                          onPressed: ()
                          {
                            rateMyApp.callEvent(RateMyAppEventType.noButtonPressed).then((_) =>
                            {
                              Navigator.pop(context),
                              Navigator.pop(context)
                            });
                          },
                        ),
                      ],
                    );
                  },
                );
                break;
            }
            return false;
          },
          ignoreIOS: false,
          // Set to false if you want to show the native Apple app rating dialog on iOS.
          dialogStyle: DialogStyle(), // Custom dialog styles.
          // onDismissed: () => rateMyApp.callEvent(RateMyAppEventType.laterButtonPressed), // Called when the user dismissed the dialog (either by taping outside or by pressing the "back" button).
          // actionsBuilder: (_) => [], // This one allows you to use your own buttons.
        );
    });

    return new Scaffold(
        appBar: new AppBar(
            title: new Text("Refuelio"),
            actions: <Widget>[
              GestureDetector(
                // When the child is tapped, show a snackbar.
                onTap: () {
                  Navigator.push(context, SlideRightRoute(page: RefillForm(new Refill()), routeName: "Refill Form"));
                },
                child: Container(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        Icons.add_box,
                        size: 24.0,
                        semanticLabel: 'Tap to add a refill',
                      ),
                      Text(" Add refill ",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0
                        ),),
                    ])
                )
              ),
              GestureDetector(
                // When the child is tapped, show a snackbar.
                  onTap: () {
                    Navigator.push(context, SlideRightRoute(page: Settings(), routeName: "Settings"));
                  },
                  child: Container(
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Icon(
                              Icons.settings,
                              size: 24.0,
                              semanticLabel: 'Settings',
                            ),
                          ])
                  )
              )],
            automaticallyImplyLeading: false
        ),
        body:SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            if(hasVehicles && utilities.getPreferenceAsBool("MultipleVehiclesEnabled", emptyValue: false))
      Row(crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
            padding: EdgeInsets.all(8.0),
            child:
            Text('Vehicle')),
              new DropdownButton(
                  hint: Text('Vehicle'),
                  items: vehicles.map((Vehicle vehicle) {
                    return new DropdownMenuItem(
                      value: vehicle,
                      child: new Text(vehicle.name),
                    );
                  }).toList(),
                  onChanged: (newValue)
                  {
                    utilities.sharedPreferences.setInt("CurrentVehicle", newValue.id);
                    setState(() {});
                  },
                  value: db.getCurrentVehicle(vehicles))]),
            if(utilities.getPreferenceAsBool("GraphsEnabled", emptyValue: true))
              Padding(
                padding: EdgeInsets.all(8.0),
                child: CarouselWithIndicator()
              ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: PetrolFillList(),
            )
          ],
        )
      )
    );
  }
}