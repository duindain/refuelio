import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:refuelio/EditVehicles.dart';
import 'SlideRightRoute.dart';
import 'Utilities.dart';
import 'models/DatabaseService.dart';
import 'models/Vehicle.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class EditVehicles extends StatefulWidget {

  EditVehicles();

  @override
  _EditVehiclesState createState() => new _EditVehiclesState();
}

class _EditVehiclesState extends State<EditVehicles>
{

  var utilities = GetIt.instance.get<Utilities>();
  var db = GetIt.instance.get<DatabaseService>();
  List<Vehicle> vehicles;
  List<Vehicle> selectedVehicles;
  bool loading = false;
  bool addVehicleEnabled = false;
  bool deleteVehicleEnabled = false;
  TextEditingController vehicleNameController = TextEditingController();

  @override
  void initState()
  {
    _asyncMethod();
    super.initState();
  }

  _asyncMethod() async {
    db.stream$.listen((x) async {
      loading = true;
      print("EditVehicles: Vehicles stream update detected");
      vehicles = await db.vehicles();
      vehicles.sort((a, b) => a.name.compareTo(b.name));
      loading = false;
      print("EditVehicles: Vehicles updated");
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context)
  {
    if(loading)
    {
      return SpinKitWave(color: Colors.orange, type: SpinKitWaveType.center);
    }
    return new Scaffold(
        appBar: new AppBar(
        title: new Text("Vehicles")
      ),
      body:
      SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child:new Padding(
      padding:new EdgeInsets.all(16.0),
      child:Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children:[
        _buildCreateVehicle(),
        if(vehicles != null && vehicles.length > 0)
          _buildTable()
    ])
  ))
    );
  }

  Widget _buildCreateVehicle()
  {
    return Column(
        children:[
        Text('Add vehicles by entering a name in the field below and clicking the Add vehicle button, they will appear in a table below. To delete a vehicle select it in the table and click the delete button.'),
        TextField(
          decoration: InputDecoration(
            labelText: "Vehicle Name"
          ),
          textInputAction: TextInputAction.done,
          controller:vehicleNameController,
          onSubmitted: (value)
          {
            //db.insertVehicle(new Vehicle(name:value, createdOn: DateTime.now()));
            //vehicleNameController.text = "";
            //setState(() {});
          }, onChanged: (value)
          {
            addVehicleEnabled = value.length > 0;
            setState(() {});
          }
        ),
          RaisedButton(
          onPressed: addVehicleEnabled == false ? null : () {
            bool hasDuplicate = false;
            for(var vehicle in vehicles)
            {
              if(vehicle.name == vehicleNameController.text)
              {
                hasDuplicate = true;
              }
            }
            if(hasDuplicate)
            {
              utilities.createAlert(context, AlertType.error, "Saving error", "You already have a vehicle called ${vehicleNameController.text}, please choose another name.");
            }
            else
            {
              db.insertVehicle(new Vehicle(name:vehicleNameController.text, createdOn: DateTime.now()));
              vehicleNameController.text = "";
              setState(() {});
            }

          },
            child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[Icon(
            Icons.add_box,
            color: Colors.white70,
            size: 24.0,
            semanticLabel: 'Add this vehicle',
            ),Text(' Add new vehicle')]),
          )
    ]);
  }

  Widget _buildTable()
  {
    return Column(
        children:[
          Padding(padding: EdgeInsets.only(top: 20.0)),
          FittedBox(
            child:DataTable(
              columns:[
                DataColumn(label: new Text('Name')),
                DataColumn(label: new Text('Created on')),
              ],
              rows:vehicles.map(((vehicle) =>
                  DataRow(
                    selected: selectedVehicles != null && selectedVehicles.contains(vehicle),
                    onSelectChanged: (selected) {
                      setState(() {
                        if(selectedVehicles == null)
                          selectedVehicles = new List<Vehicle>();
                        if(selected)
                          selectedVehicles.add(vehicle);
                        else
                          selectedVehicles.remove(vehicle);

                        deleteVehicleEnabled = selectedVehicles != null && selectedVehicles.length > 0;
                      });
                    },
                    cells: <DataCell>[
                      DataCell(Text(vehicle.name)),
                      DataCell(Text(db.dbDateTimeFormat.format(vehicle.createdOn))),
                    ],
                  )),
              ).toList(),
            ),
          ),
          RaisedButton(
            onPressed: deleteVehicleEnabled == false ? null : () {
              for(var vehicle in selectedVehicles)
              {
                db.deleteVehicle(vehicle.id);
              }
              selectedVehicles.clear();
              setState(() {});
            },
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[Icon(
                  Icons.delete,
                  color: Colors.white70,
                  size: 24.0,
                  semanticLabel: 'Delete selected vehicles',
                ),Text(' Delete selected vehicles')]),
          )
        ]);
  }

  @override
  void dispose() {
    vehicleNameController?.dispose();
    super.dispose();
  }
}