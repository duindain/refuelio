import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_it/get_it.dart';
import 'package:refuelio/charts/Chart_MonthlyCosts.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'Utilities.dart';
import 'charts/Chart_ServoCostOverTime.dart';
import 'charts/Chart_AllRefillsCostsOverTime.dart';
import 'charts/Chart_CostPerLitreOverTime.dart';
import 'charts/Chart_FuelTypeOverKMPerLitre.dart';
import 'charts/Chart_OctaneEfficiency.dart';
import 'models/DatabaseService.dart';
import 'models/Refill.dart';

class CarouselWithIndicator extends StatefulWidget {
  @override
  _CarouselWithIndicatorState createState() => _CarouselWithIndicatorState();
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicator> {

  final Widget placeholder = Container(color: Colors.grey);

  @override
  Widget build(BuildContext context) {
    var db = GetIt.instance.get<DatabaseService>();
    var utilities = GetIt.instance.get<Utilities>();
    return StreamBuilder(
        stream: db.stream$,
        builder: (BuildContext context, AsyncSnapshot streamAsync)
        {
          if (streamAsync.hasError)
          {
            return new Text("Stream Error!");
          }
          else if (streamAsync.data == null)
          {
            return new Text("Stream No Data");
          }
          else
          {
            return FutureBuilder<List<Refill>>(
                future: db.refills(),
                builder: (BuildContext context, AsyncSnapshot<List<Refill>> futureAsync)
                {
                  if (futureAsync.connectionState == ConnectionState.done)
                  {
                    List<Refill> refills = futureAsync.data;
                    var demoDataStr = "(Demo data)";
                    var demoDataDescription = "\n\nThis chart is currently displaying demonstration data, it will automatically switch to live data when you have three or more refills entered.";
                    var useSampleData = (refills == null || refills.length < 3);
                    if(useSampleData == false)
                    {
                      refills.sort((a, b) => a.refilledOn.compareTo(b.refilledOn));
                      demoDataStr = demoDataDescription = "";
                    }

                    return Stack(
                      children: [
                        CarouselSlider(
                          height: 300.0,
                          items: [
                            ChartItem(Chart_ServoCostOverTime.withData(useSampleData ? null : refills), "Servo cost per year $demoDataStr", "Shows yearly average refill costs grouped by servo, limited to the latest five years of data per servo$demoDataDescription"),
                            ChartItem(Chart_CostPerLitreOverTime.withData(useSampleData ? null : refills), "Fuel cost over time $demoDataStr", "Shows fuel cost per ${utilities.getPreferenceAsBool("Litres") ? 'litre' : 'gallon'} over time$demoDataDescription"),
                            ChartItem(Chart_FuelTypeOverKMPerLitre.withData(useSampleData ? null : refills), "Fuel efficency ${utilities.getPreferenceAsBool("Kilometers") ? 'Km/L' : 'M/G'} $demoDataStr", "This chart shows average fuel efficency in ${utilities.getPreferenceAsBool("Kilometers") ? 'Km/L' : 'M/G'} grouped by year$demoDataDescription"),
                            ChartItem(Chart_OctaneEfficiency.withData(useSampleData ? null : refills), "Octane efficiency $demoDataStr", "Shows Octane value with fuel efficiency based on ${utilities.getPreferenceAsBool("Kilometers") ? 'Km/L' : 'M/G'}$demoDataDescription"),
                            ChartItem(Chart_AllRefillsCostsOverTime.withData(useSampleData ? null : refills), "Average refill cost $demoDataStr", "Shows average refill cost over each year with coloring to indicate min, max and avg values, sizing of bubbles indicates overall cost$demoDataDescription"),
                            ChartItem(Chart_MonthlyCosts.withData(useSampleData ? null : refills), "Last years costs $demoDataStr", "The lst 12 months of fuel costs summed up per month showing in a percentage graph$demoDataDescription"),
                          ].map((chartItem) {
                            return Builder(
                              builder: (BuildContext context) {
                                return Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Expanded(
                                          child:chartItem.chart),
                                          Row(crossAxisAlignment: CrossAxisAlignment.center,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                placeholder,
                                                Text(chartItem.chartName),
                                                Padding(padding: EdgeInsets.only(left: 16.0),
                                                    child:IconButton(
                                                        icon: Icon(Icons.info),
                                                        tooltip: 'Graph information',
                                                        onPressed: () {
                                                          Alert(context: context, style: utilities.alertStyle, type: AlertType.info, title: "Chart info", desc: chartItem.chartInfo, buttons: [
                                                            DialogButton(
                                                              child: Text(
                                                                "Close",
                                                                style: TextStyle(color: Colors.white, fontSize: 20),
                                                              ),
                                                              onPressed: () => Navigator.pop(context),
                                                              width: 120,
                                                            )
                                                          ],).show();
                                                        }
                                                        )
                                                )
                                          ])
                                    ])
                                );
                              },
                            );
                          }).toList(),
                        )]
                    );
                  }
                  else
                  {
                    return SpinKitWave(color: Colors.orange, type: SpinKitWaveType.center);
                  }
                });
          }
        });
  }
}

class ChartItem
{
  final Widget chart;
  final String chartName;
  final String chartInfo;
  ChartItem(this.chart, this.chartName, this.chartInfo);
}