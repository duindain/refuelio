import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get_it/get_it.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'GetScan.dart';
import 'import/CsvImportForm.dart';
import 'SlideRightRoute.dart';
import 'Utilities.dart';
import 'models/DatabaseService.dart';
import 'models/Refill.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

import 'models/Vehicle.dart';

// Create a Form widget.
class RefillForm extends StatefulWidget {
  final Refill refill;

  RefillForm(this.refill);

  @override
  RefillFormState createState() {
    return RefillFormState(refill);
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class RefillFormState extends State<RefillForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  final now = DateTime.now();
  final Refill refill;
  bool loading = false;
  List<Vehicle> vehicles;
  Vehicle selectedVehicle;
  Refill lastRefill;
  var dbAccess = GetIt.instance.get<DatabaseService>();
  var utilities = GetIt.instance.get<Utilities>();

  RefillFormState(this.refill);

  @override
  void initState() {
    _asyncMethod();
    super.initState();
  }

  _asyncMethod() async {
    setState(() {
    dbAccess.stream$.listen((x) async {

      loading = true;
      print("RefillForm: Vehicles stream update detected");
      vehicles = await dbAccess.vehicles();
      if(vehicles != null && vehicles.length > 0)
      {
        vehicles.sort((a, b) => a.name.compareTo(b.name));
        if(refill.vehicleId == null)
        {
          selectedVehicle = vehicles.firstWhere((a) => a.id == refill.vehicleId, orElse: () => dbAccess.getCurrentVehicle(vehicles));
        }
      }
      refill.vehicleId = selectedVehicle?.id ?? 0;
      var refills = await dbAccess.refills();
      if(refills != null && refills.length > 0)
      {
        refills.sort((a, b) => a.refilledOn.compareTo(b.refilledOn));
        lastRefill = refills.last;
      }
      loading = false;
      print("RefillForm: Vehicles updated");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if(loading)
    {
      return SpinKitWave(color: Colors.orange, type: SpinKitWaveType.center);
    }
    var title = "Adding refill";
    if(refill.id != null)
    {
      title = "Editing refill";
    }

    if(refill.servo == null || refill.servo.isEmpty == true)
      refill.servo = utilities.getPreferenceAsString("ServoName", emptyValue: 'local');
    if(refill.octane == null || refill.octane.isEmpty == true)
      refill.octane = utilities.getPreferenceAsString("Octane", emptyValue: '98.0');

    return new Scaffold(
        appBar: AppBar(
          title: new Text(title),
            actions: <Widget>[
              GestureDetector(
              // When the child is tapped, show a snackbar.
                onTap: () {
                  Navigator.push(context, SlideRightRoute(page: CsvImportForm(), routeName: "Csv Import Form"));
                },
                child: Container(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Icon(
                            Icons.grid_on,
                            size: 24.0,
                            semanticLabel: 'Csv import',
                          ),
                          Text(" Csv import ",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20.0
                            ),),
                        ])
                  )
              ),
          ]
        ),
        body: Form(
            key: _formKey,
            child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child:Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  if(vehicles != null && vehicles.length > 1 && utilities.getPreferenceAsBool("MultipleVehiclesEnabled", emptyValue: false))
                Container(
            padding: EdgeInsets.only(top: 8.0),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Icon(
                  Icons.directions_car,
                  color: Colors.lightBlue,
                  size: 24.0,
                  semanticLabel: 'Which vehicle to assign the refill to',
                ),
                Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(left: 16.0),
                          child: Text('Select the vehicle for this refill')),
                      Padding(
                          padding: EdgeInsets.only(left: 16.0),
                          child: new DropdownButton(
                      hint: Text('Select a vehicle'),
                      items: vehicles.map((Vehicle vehicle) {
                        return new DropdownMenuItem(
                          value: vehicle,
                          child: new Text(vehicle.name),
                        );
                      }).toList(),
                      onChanged: (selected)
                      {
                        selectedVehicle = selected;
                        refill.vehicleId = selected.id;
                        setState(() {
                          //utilities.sharedPreferences.setInt("CurrentVehicle", selected.id);

                        });
                      },
                      value: selectedVehicle)),])
              ]
          )),

                  TextFormField(
                    decoration: InputDecoration(
                        labelText: 'Enter the servo name, or leave the default value',
                        icon: Icon(Icons.local_gas_station)),
                    initialValue: refill.servo ?? "",
                    onSaved: (val) => setState(() => refill.servo = val),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter a servo name or Any'; //database default servo name setting
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    keyboardType: TextInputType.numberWithOptions(decimal: true),
                    initialValue: refill.litres?.toString() ?? "",
                    onSaved: (val) => setState(() => refill.litres = double.parse(val)),
                    decoration: InputDecoration(
                        labelText: 'Enter ${utilities.getPreferenceAsBool("Litres") ? 'litres' : 'gallons'} filled',
                        //database litres of gallons
                        icon: Icon(Icons.local_car_wash)),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter ${utilities.getPreferenceAsBool("Litres") ? 'litres' : 'gallons'} filled';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    keyboardType: TextInputType.numberWithOptions(decimal: true),
                    initialValue: refill.kmPerLitre?.toString() ?? "",
                    onSaved: (val) => setState(() => refill.kmPerLitre = double.parse(val)),
                    decoration: InputDecoration(
                        labelText: 'Enter previous fuel efficency ${utilities.getPreferenceAsBool("Kilometers") ? 'Km/L' : 'M/G'}',
                        //database L or G
                        icon: Icon(Icons.multiline_chart)),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter ${utilities.getPreferenceAsBool("Kilometers") ? 'Km/L' : 'M/G'} value';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    keyboardType: TextInputType.numberWithOptions(decimal: true),
                    initialValue: refill.cost?.toString() ?? "",
                    onSaved: (val) => setState(() => refill.cost = double.parse(val)),
                    decoration: InputDecoration(
                      labelText: 'Enter refill cost',
                      icon: Icon(Icons.attach_money),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter refill cost';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    initialValue: refill.octane ?? "",
                    onSaved: (val) => setState(() => refill.octane = val),
                    decoration: InputDecoration(
                        labelText:
                            'Enter your fuel Octane level, or leave the default value',
                        icon: Icon(Icons.equalizer)),
                    validator: (value) {
                      return null;
                    },
                  ),
                  Container(
                      padding: EdgeInsets.only(top: 8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Icon(
                            Icons.date_range,
                            color: Colors.pink,
                            size: 24.0,
                            semanticLabel: 'Refill date and time icon',
                          ),
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.only(left: 16.0),
                                    child: Text('Refill date and time')),
                                FlatButton(
                                    onPressed: () {
                                      DatePicker.showDateTimePicker(
                                        context,
                                        showTitleActions: true,
                                        onConfirm: (date) {
                                          refill.refilledOn = date;
                                          setState(() {
                                          });
                                        },
                                        currentTime: refill.refilledOn ?? now,
                                      );
                                    },
                                    child: Text(
                                      dbAccess.dbDateTimeFormat.format(refill.refilledOn ?? now),
                                    )),
                                /*
                   This picker is better but doesnt work in nested Column and Row
                      DateTimeField(
                        format: format,
                        initialValue: DateTime.now(),
                        onShowPicker: (context, currentValue) async {
                          final date = await showDatePicker(
                              context: context,
                              firstDate: DateTime(1900),
                              initialDate: currentValue ?? DateTime.now(),
                              lastDate: DateTime(2100));
                          if (date != null)
                          {
                            final time = await showTimePicker(
                              context: context,
                              initialTime:
                              TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                            );
                            return DateTimeField.combine(date, time);
                          }
                          else
                          {
                            return currentValue;
                          }
                        },
                      )
                      */
                                // )
                              ])
                        ],
                      )),
      Visibility (
        visible: utilities.getPreferenceAsBool("OCREnabled", emptyValue: false),
        child:Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 4.0),
                          child:  RaisedButton(
                              onPressed: () {
                                Navigator.push(context, SlideRightRoute(page: GetScan(), routeName: "Image Scan or load from gallery"));
                              },
                              child:Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[ Icon(
                                    Icons.settings_overscan,
                                    color: Colors.white70,
                                    size: 24.0,
                                    semanticLabel: 'Refill date and time icon',
                                  ),Text(' Scan Reciept')])
                          )
                      )])),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: RaisedButton(
                      onPressed: () {
                        // Validate returns true if the form is valid, or false
                        // otherwise.
                        if (_formKey.currentState.validate()) {
                          var form = _formKey.currentState;
                          form.save();
                          refill.createdOn = DateTime.now();
                          refill.refilledOn = refill.refilledOn ?? now;
                          if(refill.id != null && refill.id > 0)
                            dbAccess.updateRefill(refill);
                          else
                            dbAccess.insertRefill(refill);
                          if(lastRefill != null)
                          {
                            var costMoreOrLess = lastRefill.cost == refill.cost ? "the same" : lastRefill.cost < refill.cost ? "more than" : "less than";
                            var difference = utilities.getDifference(lastRefill.cost, refill.cost);
                            var refillSummary = "This refill cost $costMoreOrLess the last by \$${difference}";

                            var filledMoreOrLess = lastRefill.litres == refill.litres ? "the same" : lastRefill.litres < refill.litres ? "more than" : "less than";
                            var litresDifference = utilities.getDifference(lastRefill.litres, refill.litres);
                            refillSummary += "\n$litresDifference ${utilities.getPreferenceAsBool("Litres") ? 'litres' : 'gallons'} $filledMoreOrLess last time";

                            if(lastRefill.kmPerLitre > 0 && refill.kmPerLitre > 0)
                            {
                              var efficencyMoreOrLess =  lastRefill.kmPerLitre == refill.kmPerLitre ? "the same" : lastRefill.kmPerLitre > refill.kmPerLitre ? "better" : "worse";
                              var efficencyDifference = utilities.getDifference(lastRefill.kmPerLitre, refill.kmPerLitre);
                              var name = selectedVehicle == null ? "The vehicle" : selectedVehicle.name;
                              refillSummary += "\n$name has $efficencyMoreOrLess efficency this refill by $efficencyDifference ${utilities.getPreferenceAsBool("Kilometers") ? 'Km/L' : 'M/G'}";
                            }
                            Alert(context:context, style: utilities.alertStyle, type: AlertType.success, title: "Refill report", desc: refillSummary, buttons: [
                              DialogButton(
                                width: 120,
                                child: Text("Close", style: TextStyle(color: Colors.white, fontSize: 20),),
                                onPressed: () =>
                                {
                                  //Close Dialog
                                  Navigator.pop(context),
                                  //Close Import page
                                  Navigator.pop(context),
                                },
                              )
                            ],).show();
                          }
                          else
                          {
                            Navigator.pop(context);
                          }
                        }
                      },
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[Icon(
                            Icons.save,
                            color: Colors.white70,
                            size: 24.0,
                            semanticLabel: 'Refill date and time icon',
                          ),Text(' Save')]),
                    ),
                  ),
              Visibility (
                  visible: refill.id != null,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: RaisedButton(
                    onPressed: () {
                      dbAccess.deleteRefill(refill.id);
                      Navigator.pop(context);
                    },
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[Icon(
                      Icons.delete,
                      color: Colors.white70,
                      size: 24.0,
                      semanticLabel: 'Delete this refill',
                    ),Text(' Delete')]),
                  )
                  )
                ),

                   ],
                  )

                ],
              ),
            )
            )
        )
    );
  }
}
