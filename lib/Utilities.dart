import 'package:flutter/material.dart';
import 'package:flutter_crashlytics/flutter_crashlytics.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';
import 'package:uuid/uuid_util.dart';

typedef FancyButtonCallback = void Function();

class Utilities
{
  var uuid = new Uuid(options: {
    'grng': UuidUtil.cryptoRNG
  });
  SharedPreferences sharedPreferences;
  DateFormat formatter = new DateFormat('yyyy-MM-dd H:m:s');
  String baseUrl = "https://rmu.no-ip.com/refuelio/";
  String featuresUrl = "https://refuelio-d8091.web.app/features.json";

  Future initilise() async {
    sharedPreferences = await SharedPreferences.getInstance();
    initiliseSharedPreferences(sharedPreferences);
    //sharedPreferences.setString('UpdateFeatures', null);
    var featuresUpdated = getPreferenceAsString("UpdateFeatures");
    if(featuresUpdated.isEmpty || formatter.parse(featuresUpdated).isBefore(DateTime.now()))
    {
      print("Utilities.initilise: Feature check duration elapsed, retrieving features");
      await fetchFeatures();
    }
    var appLaunches = getPreferenceAsInt("AppLaunches") + 1;
    print("Utilities.initilise: AppLaunches set to $appLaunches");
    sharedPreferences.setInt("AppLaunches", appLaunches);
  }

  double getDifference(double a, double b)
  {
      var difference = a - b;
      if(difference < 0)
        difference = difference * -1;
      return difference;
  }

  String getPreferenceAsString(String key, {String emptyValue = ""})
  {
    return sharedPreferences.containsKey(key) ? sharedPreferences.getString(key) : emptyValue;
  }

  int getPreferenceAsInt(String key, {int emptyValue = 0})
  {
    return sharedPreferences.containsKey(key) ? sharedPreferences.getInt(key) : emptyValue;
  }

  bool getPreferenceAsBool(String key, {bool emptyValue = false})
  {
    return sharedPreferences.containsKey(key) ? sharedPreferences.getBool(key) : emptyValue;
  }

  Future fetchFeatures() async {
    var succeeded = false;
    try
    {
      var response = await http.get(featuresUrl);
      if(response.statusCode == 200)
      {
        var features = json.decode(response.body);
        print("Utilities.fetchFeatures: Downloaded features : $features");
        sharedPreferences.setBool("OCREnabled", features["ocrScanning"].toLowerCase() == 'true');
        sharedPreferences.setBool("OffsiteBackup", features["offsiteBackup"].toLowerCase() == 'true');
        //Dont update again until tomorrow
        sharedPreferences.setString('UpdateFeatures', formatter.format(DateTime.now().add(new Duration(days : 1))));
        succeeded = true;
      }
    } on Exception catch (exception, stackTrace) {
      FlutterCrashlytics().logException(exception, stackTrace);
    } catch (error) {
      FlutterCrashlytics().log(error);
    }
    finally
    {
      if(succeeded == false)
      {
        sharedPreferences.setBool("OCREnabled", false);
        sharedPreferences.setBool("OffsiteBackup", false);
        //Dont update again until tomorrow
        sharedPreferences.setString('UpdateFeatures', formatter.format(DateTime.now().add(new Duration(days : 1))));
      }
    }
  }

  void initiliseSharedPreferences(SharedPreferences sharedPreferences)
  {
    if (sharedPreferences.containsKey("Octane") == false)
    {
      print("Utilities.initiliseSharedPreferences: Setting default shared preferences");
      sharedPreferences.setBool("OffsiteBackup", false);
      sharedPreferences.setString("ServoName", "local");
      sharedPreferences.setString("ClientId", uuid.v4());
      sharedPreferences.setString("Octane", "98.0");
      sharedPreferences.setString('UpdateFeatures', formatter.format(DateTime.now().add(new Duration(seconds:-1))));
      sharedPreferences.setBool("DarkTheme", true);
      sharedPreferences.setBool("GraphsEnabled", true);
      sharedPreferences.setBool("Kilometers", true);
      sharedPreferences.setBool("Litres", true);
      sharedPreferences.setBool("OCREnabled", false);
      sharedPreferences.setBool("MultipleVehiclesEnabled", false);
      sharedPreferences.setInt("CurrentVehicle", 0);
      sharedPreferences.setInt("AppLaunches", 0);
    }
  }

  String truncate(int cutoff, String myString, {String concatText = ''}) {
    return (myString.length <= cutoff)
        ? myString
        : '${myString.substring(0, cutoff)}${concatText != null ? concatText : ''}';
  }

  bool isJson(String text)
  {
    return text.startsWith("{");
  }

  Widget fancyPantsButton(BuildContext context, IconData icon, String text, String screenReaderText, FancyButtonCallback callback, {Color border = Colors.red, Color background, Color textColor = Colors.white})
  {
    if(background == null)
    {
      background = Theme.of(context).primaryColor;
    }
    return RaisedButton.icon(
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(12.0),
            side: BorderSide(color: border)
        ),
        elevation: 4.0,
        icon: Icon(icon, semanticLabel: screenReaderText,),
        color: Theme.of(context).primaryColor,
        onPressed: callback,
        label: Text(text, style: TextStyle(color: textColor, fontSize: 16.0))
    );
  }

  final AlertStyle alertStyle = AlertStyle(
    animationType: AnimationType.fromTop,
    isCloseButton: false,
    isOverlayTapDismiss: false,
    descStyle: TextStyle(fontWeight: FontWeight.bold),
    animationDuration: Duration(milliseconds: 400),
    alertBorder: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(0.0),
      side: BorderSide(
        color: Colors.grey,
      ),
    ),
    titleStyle: TextStyle(
      color: Colors.red,
    ),
  );

  void createAlert(BuildContext context, AlertType alertType, String title, String text)
  {
    Alert(context: context, style: alertStyle, type: alertType, title: title, desc: text, buttons: [
      DialogButton(
        child: Text(
          "Close",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],).show();
  }
}