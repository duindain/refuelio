import 'package:flutter/material.dart';

class ImportStepOne extends StatelessWidget {

    ImportStepOne();

    @override
    Widget build(BuildContext context)
    {
      return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children:[
            Text('Import lets you import a spreadsheet stored in Csv format into the app. You can set some parameters in the following wizard to help make the import sucseed'),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            Text('Before the import is finished the wizard will show what will be imported.'),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            Text('Press next when ready to move to the next step..')
          ]);
    }
}