import 'package:flutter/material.dart';

typedef StepTwoCallback = void Function(String eol, String dataDelimiter, String dateTimeFormat, bool ignoreFirstRow);

class ImportStepTwo extends StatefulWidget {
  String eol = '\n';
  String dataDelimiter;
  String dateTimeFormat;
  bool ignoreFirstRow = false;

  final StepTwoCallback onStepCallback;

  ImportStepTwo(this.eol, this.dataDelimiter, this.dateTimeFormat, this.ignoreFirstRow, this.onStepCallback);

@override
  _ImportStepTwoState createState() => new _ImportStepTwoState();
}

class _ImportStepTwoState extends State<ImportStepTwo> {

  String selectedEolDelimiter;
  String selectedDataDelimiter;
  final List<String> eolDelimiters = ['Carridge return \\n', 'Carridge return, line feed \\n\\r'];
  final List<String> dataDelimiters = ['Comma ,', 'Ampersan ;', 'Colon :', 'Dot .'];
  final List<String> dateTimeFormats = ['yyyy-MM-dd H:m:s', 'yyyy-MM-dd', 'yyyy-dd-MM H:m:s', 'yyyy-dd-MM'];

  @override
  void initState()
  {
    super.initState();
    selectedEolDelimiter = eolDelimiters[widget.eol.endsWith('\n') ? 0 : 1];
    selectedDataDelimiter = ConvertFromDataDelimiter(widget.dataDelimiter);
  }

    @override
  Widget build(BuildContext context) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children:[
          Text('Please select an end of line delimiter or leave to use the default of \\n'),
          Padding(padding: EdgeInsets.only(top: 20.0)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children:[
              new DropdownButton(
                hint: Text('Select the delimiter for the import'),
                items: eolDelimiters.map((String val) {
                  return new DropdownMenuItem(
                    value: val,
                    child: new Text(val),
                  );
                }).toList(),
                onChanged: (newValue)
                {
                  selectedEolDelimiter = newValue;
                  widget.eol = selectedEolDelimiter.startsWith('Carridge return, ') ? '\n\r' : '\n';

                  widget.onStepCallback(widget.eol, widget.dataDelimiter, widget.dateTimeFormat, widget.ignoreFirstRow);
                  setState(() {});
                },
                value:selectedEolDelimiter)
            ]),
          Text('Please select an data delimiter or leave to use the default of Comma ,'),
          Padding(padding: EdgeInsets.only(top: 20.0)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children:[
              new DropdownButton(
                hint: Text('Select the delimiter for the import'),
                items: dataDelimiters.map((String val) {
                  return new DropdownMenuItem(
                    value: val,
                    child: new Text(val),
                  );
                }).toList(),
                onChanged: (newValue)
                {
                  selectedDataDelimiter = newValue;
                  widget.dataDelimiter = ConvertToDataDelimiter(selectedDataDelimiter);

                  widget.onStepCallback(widget.eol, widget.dataDelimiter, widget.dateTimeFormat, widget.ignoreFirstRow);
                  setState(() {});
                },
                value:selectedDataDelimiter)
            ]),
          Padding(padding: EdgeInsets.only(top: 20.0)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children:[
              Text('Does your Csv contain a header row?'),
              new Checkbox(
                value: widget.ignoreFirstRow,
                onChanged: (value)
                {
                  widget.ignoreFirstRow = value;

                  widget.onStepCallback(widget.eol, widget.dataDelimiter, widget.dateTimeFormat, widget.ignoreFirstRow);
                  setState(() {});
                }),
            ]),
          Padding(padding: EdgeInsets.only(top: 20.0)),
          Text('Please enter the DateTime format for any dates in your Csv or leave empty to accept the default format of ${widget.dateTimeFormat}'),
          Padding(padding: EdgeInsets.only(top: 20.0)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children:[
              new DropdownButton(
                hint: Text('Select a datetime format for the import'),
                items: dateTimeFormats.map((String val) {
                  return new DropdownMenuItem(
                    value: val,
                    child: new Text(val),
                  );
                }).toList(),
                onChanged: (newValue)
                {
                  widget.dateTimeFormat = newValue;

                  widget.onStepCallback(widget.eol, widget.dataDelimiter, widget.dateTimeFormat, widget.ignoreFirstRow);
                  setState(() {});
                },
                value:widget.dateTimeFormat)
            ])
        ]);
  }

  String ConvertToDataDelimiter(String selected)
  {
    var delimiter = '';
    if(selected.startsWith('Comma'))
      delimiter = ',';
    else if(selected.startsWith('Ampersan'))
      delimiter = ';';
    else if(selected.startsWith('Colon'))
      delimiter = ':';
    else if(selected.startsWith('Dot'))
      delimiter = '.';

    return delimiter;
  }

  String ConvertFromDataDelimiter(String selected)
  {
    var delimiter = '';
    if(selected == ',')
      delimiter = 'Comma ,';
    else if(selected== ';')
      delimiter = 'Ampersan ;';
    else if(selected== ':')
      delimiter = 'Colon :';
    else if(selected == '.')
      delimiter = 'Dot .';

    return delimiter;
  }
}