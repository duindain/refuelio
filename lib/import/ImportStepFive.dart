import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

typedef StepFiveCallback = void Function(List<List<dynamic>> rowsSelectedToImport, bool isNextButtonDisabled);

class ImportStepFive extends StatefulWidget {
  List<List<dynamic>> rowsToImport;
  List<List<dynamic>> fieldsData;
  Map<String, int> columns;
  bool isNextButtonDisabled;
  String litresColumn;
  String kmsColumn;

  final StepFiveCallback onStepCallback;

  ImportStepFive(this.fieldsData, this.rowsToImport, this.columns, this.isNextButtonDisabled, this.litresColumn, this.kmsColumn, this.onStepCallback);

@override
  _ImportStepFiveState createState() => new _ImportStepFiveState();
}

class _ImportStepFiveState extends State<ImportStepFive> {

  @override
  void initState()
  {
    super.initState();
  }

    @override
  Widget build(BuildContext context) {
      return Column(
          children:[
            Text('Select rows to be imported from the following table, import will be run when clicking Next.'),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            FittedBox(
              child:DataTable(
                columns:[
                  if(widget.columns["Servo"] != -1)
                    DataColumn(label: new Text('Servo')),
                  if(widget.columns[widget.litresColumn] != -1)
                    DataColumn(label: new Text(widget.litresColumn)),
                  if(widget.columns[widget.kmsColumn] != -1)
                    DataColumn(label: new Text(widget.kmsColumn)),
                  if(widget.columns["Octane"] != -1)
                    DataColumn(label: new Text('Octane')),
                  if(widget.columns["Cost"] != -1)
                    DataColumn(label: new Text('Cost')),
                  if(widget.columns["Date"] != -1)
                    DataColumn(label: new Text('Date')),
                ],
                rows:widget.fieldsData.map(((import) =>
                    DataRow(
                      selected: widget.rowsToImport.contains(import),
                      onSelectChanged: (selected) {
                        setState(() {
                          if(selected)
                            widget.rowsToImport.add(import);
                          else
                            widget.rowsToImport.remove(import);
                          widget.isNextButtonDisabled = widget.rowsToImport.length == 0;
                          widget.onStepCallback(widget.rowsToImport, widget.isNextButtonDisabled);
                        });
                      },
                      cells: <DataCell>[
                        if(widget.columns["Servo"] != -1)
                          DataCell(Text(import[widget.columns["Servo"]].toString())),
                        if(widget.columns[widget.litresColumn] != -1)
                          DataCell(Text(import[widget.columns[widget.litresColumn]].toString())),
                        if(widget.columns[widget.kmsColumn] != -1)
                          DataCell(Text(import[widget.columns[widget.kmsColumn]].toString())),
                        if(widget.columns["Octane"] != -1)
                          DataCell(Text(import[widget.columns["Octane"]].toString())),
                        if(widget.columns["Cost"] != -1)
                          DataCell(Text(import[widget.columns["Cost"]].toString())),
                        if(widget.columns["Date"] != -1)
                          DataCell(Text(import[widget.columns["Date"]].toString())),
                      ],
                    )),
                ).toList(),
              ),
            ),
          ]);
  }
}