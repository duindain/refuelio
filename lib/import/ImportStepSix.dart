import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import '../Utilities.dart';
import '../models/DatabaseService.dart';
import '../models/Refill.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:intl/intl.dart';

import 'ImportAlert.dart';

typedef StepCallback = void Function(ImportAlert importAlert);

class ImportStepSix extends StatefulWidget {
  final Map<String, int> columns;
  final List<List<dynamic>> rowsToImport;
  final StepCallback onStepCallback;
  String dateTimeFormat;
  String litresColumn;
  String kmsColumn;

  ImportStepSix(this.columns, this.rowsToImport, this.dateTimeFormat, this.litresColumn, this.kmsColumn, this.onStepCallback);

@override
  _ImportStepSixState createState() => new _ImportStepSixState();
}

class _ImportStepSixState extends State<ImportStepSix> {

  var utilities = GetIt.instance.get<Utilities>();
  var dbAccess = GetIt.instance.get<DatabaseService>();
  ImportAlert importAlert;

  bool processedImport;

  @override
  void initState()
  {
    super.initState();
    processedImport = false;
    print("initState processedImport false");
  }

    @override
  Widget build(BuildContext context) {
      return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children:[
            Text('Click the button below to import the ${widget.rowsToImport.length} refill${widget.rowsToImport.length > 1 ? "s" : ""}.'),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            Text('If any errors are encounted the entire import is stopped and you can go back and correct the issue by clicking Try Again.'),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            Text('If successful the wizard will close and return to the main Dashboard automatically.'),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            new RaisedButton(
              onPressed: () => _processImport(),
              child: new Text("Process import"),
            ),
          ]);


      /*

    print("processedImport $processedImport");

    if(processedImport == false)
    {
      var imported = _importRefills();

      if(imported)
      {
        return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children:[
              Text('Success', style: TextStyle(fontSize: 16.0)),
              Padding(padding: EdgeInsets.only(top: 20.0)),
              Text('Completed importing ${widget.rowsToImport.length} refills.'),
              Padding(padding: EdgeInsets.only(top: 20.0)),
              Text('Click next to complete the import and return to the main app page.'),
            ]);
      }
      else
      {
        widget.onStepCallback(true, importAlert);
      }
    }

    return new Container();

    else
    {
      return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children:[
            Text('Parsing Error', style: TextStyle(fontSize: 16.0)),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            Text('Failed to importing ${widget.rowsToImport.length} refills.'),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            Text('Click next to complete the import and return to the main app page.'),
          ]
      );
    }
    */
  }
  void _processImport()
  {
    var imported = _importRefills();

    if(imported)
    {
      //Close Dialog
      //Navigator.pop(context);
      //Close Import page
      Navigator.pop(context);
      //Close Refill page
      Navigator.pop(context);
    }
    else
    {
      widget.onStepCallback(importAlert);
    }
  }

  bool _importRefills()
  {
    processedImport = true;

    var index = 1;
    var refills = new List<Refill>();
    for(var row in widget.rowsToImport)
    {
      var refill = new Refill();

      refill.createdOn = DateTime.now();
      refill.servo = widget.columns["Servo"] != -1 ? row[widget.columns["Servo"]] : utilities.getPreferenceAsString("ServoName");

      var litres = _parseDouble(row, widget.columns[widget.litresColumn], index);
      if(litres == -1)
        return false;
      refill.litres = litres;

      var kmPerLitre = _parseDouble(row, widget.columns[widget.kmsColumn], index);
      if(kmPerLitre == -1)
        return false;
      refill.kmPerLitre = kmPerLitre;
      refill.octane = widget.columns["Octane"] != -1 ? row[widget.columns["Octane"]].toString() : utilities.getPreferenceAsString("Octane");

      var cost = _parseDouble(row, widget.columns["Cost"], index);
      if(cost == -1)
        return false;
      refill.cost = cost;

      try
      {
        refill.refilledOn = widget.columns["Date"] != -1 ? DateFormat(widget.dateTimeFormat).parse(row[widget.columns["Date"]].toString()) : DateTime.now();
      }
      on FormatException
      {
        importAlert = new ImportAlert(AlertType.error, "Parsing error", "Import cancelled, failed to parse DateTime on row $index, ${row[widget.columns["Date"]].toString()} using format ${widget.dateTimeFormat}, please try a different DateTime format or change the data in the Csv file to match the format selection.", 2);
        return false;
      }
      refills.add(refill);
      index++;
    }
    for(var refill in refills)
    {
      dbAccess.insertRefill(refill);
    }
    return true;
  }

  double _parseDouble(List<dynamic> data, int column, int index)
  {
    try
    {
      return column != -1 ? double.parse(data[column].toString()) : 0.0;
    }
    on FormatException
    {
      importAlert =  new ImportAlert(AlertType.error, "Parsing error", "Import cancelled, failed to parse double on row $index, ${data[column].toString()}, please change the data in the Csv file to a valid double value.", 3);
      return -1;
    }
  }
}