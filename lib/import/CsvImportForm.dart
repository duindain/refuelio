import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:refuelio/import/ImportStepSix.dart';
import 'package:refuelio/import/ImportStepTwo.dart';
import '../Utilities.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'ImportAlert.dart';
import 'ImportStepFive.dart';
import 'ImportStepFour.dart';
import 'ImportStepOne.dart';
import 'ImportStepThree.dart';

typedef StepCallback = void Function(int step);

class CsvImportForm extends StatefulWidget {
  @override
  _CsvImportFormState createState() => new _CsvImportFormState();
}

class _CsvImportFormState extends State<CsvImportForm> {
  List<List<dynamic>> fieldsData;
  var utilities = GetIt.instance.get<Utilities>();
  Map<String, int> columns;
  Map<String, String> columnsDisplay;
  int step = 1;
  String eol = '\n';
  String dataDelimiter = ',';
  String dateTimeFormat = "yyyy-MM-dd H:m:s";
  bool ignoreFirstRow = false;
  bool _isPrevButtonDisabled = true;
  bool _isNextButtonDisabled = false;
  String litresColumn;
  String kmsColumn;
  List<List<dynamic>> rowsToImport = new List<List<dynamic>>();

  @override
  void initState() {
    super.initState();
    litresColumn = utilities.getPreferenceAsBool("Litres") ? 'Litres' : 'Gallons';
    kmsColumn = utilities.getPreferenceAsBool("Kilometers") ? 'Km/L' : 'M/G';
  }

  /*
  Step 1 Intro
  Step 2 Set File parsing parameters
  Step 3 Select file
  Step 4 Report on loaded data and Drag and drop columns
  Step 5 show what we are going to import in a table
  Step 6 import or error
  Step 7
   */

  @override
  Widget build(BuildContext context) {
    var prevStepWidget = utilities.fancyPantsButton(context, Icons.navigate_before, "Prev", "Navigate to previous step in wizard", _isPrevButtonDisabled ? null : ()
    {
      setState(()
      {
        if(step > 1)
        {
          step--;
          _isPrevButtonDisabled = step == 1;
          _isNextButtonDisabled = false;
          if(step == 5 || step == 4)
            _isNextButtonDisabled = rowsToImport.length == 0;
          if(step == 3)
            _isNextButtonDisabled = fieldsData == null || fieldsData.length == 0;
        }
      });
    });

    var nextStepWidget = utilities.fancyPantsButton(context, Icons.navigate_next, "Next", "Navigate to next step in wizard", _isNextButtonDisabled ? null : () =>
    {
      setState(()
      {
        step++;
        if(step > 1)
          _isPrevButtonDisabled = false;
        if(step == 5 || step == 4)
          _isNextButtonDisabled = rowsToImport.length == 0;
        if(step == 3)
          _isNextButtonDisabled = fieldsData == null || fieldsData.length == 0;
        if(step == 7)
        {
          //Close Import page
          Navigator.pop(context);
          //Close Refill page
          Navigator.pop(context);
        }
      })
    });

    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Csv Import Wizard")
        ),
        body:
          Padding(
            padding: EdgeInsets.only(top:20, left: 10.0, right: 10.0),
            child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            if(step == 1)
              new ImportStepOne(),
            if(step == 2)
              new ImportStepTwo(eol, dataDelimiter, dateTimeFormat, ignoreFirstRow, (String eol, String dataDelimiter, String dateTimeFormat, bool ignoreFirstRow)
              {
                this.eol = eol;
                this.dataDelimiter = dataDelimiter;
                this.dateTimeFormat = dateTimeFormat;
                this.ignoreFirstRow = ignoreFirstRow;
              }),
            if(step == 3)
              new ImportStepThree(fieldsData, eol, dataDelimiter, dateTimeFormat, ignoreFirstRow, _isNextButtonDisabled, (List<List<dynamic>> fieldsData, bool isNextButtonDisabled, ImportAlert importAlert)
              {
                this.fieldsData = fieldsData;
                this._isNextButtonDisabled = isNextButtonDisabled;
                setState(() {});
                if(importAlert != null)
                  _showDialog(importAlert);
              }),
            if(step == 4)
              new ImportStepFour(fieldsData, columns, columnsDisplay, _isNextButtonDisabled, litresColumn, kmsColumn, (List<List<dynamic>> fieldsData, Map<String, int> columns, Map<String, String> columnsDisplay, bool isNextButtonDisabled)
              {
                this.fieldsData = fieldsData;
                this.columns = columns;
                this.columnsDisplay = columnsDisplay;
                this._isNextButtonDisabled = isNextButtonDisabled;
                setState(() {});
              }),
            if(step == 5)
              new ImportStepFive(fieldsData, rowsToImport, columns, _isNextButtonDisabled, litresColumn, kmsColumn, (List<List<dynamic>> rowsSelectedToImport, bool isNextButtonDisabled)
              {
                setState(() {
                  rowsToImport = rowsSelectedToImport;
                  _isNextButtonDisabled = isNextButtonDisabled;
                });
              }),
            if(step == 6)
              new ImportStepSix(columns, rowsToImport, dateTimeFormat, litresColumn, kmsColumn, (ImportAlert importAlert)
              {
                setState(() {});
                if(importAlert != null)
                  _showDialog(importAlert);
              }),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children:[
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: prevStepWidget
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: nextStepWidget
                  ),]
              )
            )
          ])
        ));
  }

  void _showDialog(ImportAlert importAlert)
  {
    print('_showDialog title: ${importAlert.title}, desc: ${importAlert.message}, stepToGoTo: ${importAlert.goToStep}');
    Alert(context:context, style: utilities.alertStyle, type: importAlert.alertType, title: importAlert.title, desc: importAlert.message, buttons: [
      DialogButton(
        width: 120,
        child: Text("Try Again", style: TextStyle(color: Colors.white, fontSize: 20)),
        onPressed: () =>
          setState(()
          {
            step = importAlert.goToStep;
            //Close dialog
            Navigator.pop(context);
          }),
      ),
      DialogButton(
        width: 120,
        child: Text("Cancel Import", style: TextStyle(color: Colors.white, fontSize: 20),),
        onPressed: () =>
        {
          //Close Dialog
          Navigator.pop(context),
          //Close Import page
          Navigator.pop(context),
          //Close Refill page
          Navigator.pop(context),
        },
      )
    ],).show();
  }
}