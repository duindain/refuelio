import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:csv/csv.dart';
import 'dart:convert';
import 'ImportAlert.dart';

typedef StepThreeCallback = void Function(List<List<dynamic>> fieldsData, bool isNextButtonDisabled, ImportAlert importAlert);

class ImportStepThree extends StatefulWidget {
  List<List<dynamic>> fieldsData;
  String eol;
  String dataDelimiter;
  String dateTimeFormat;
  bool ignoreFirstRow = false;
  bool isNextButtonDisabled;

  final StepThreeCallback onStepCallback;

  ImportStepThree(this.fieldsData, this.eol, this.dataDelimiter, this.dateTimeFormat, this.ignoreFirstRow, this.isNextButtonDisabled, this.onStepCallback);

@override
  _ImportStepThreeState createState() => new _ImportStepThreeState();
}

class _ImportStepThreeState extends State<ImportStepThree> {

  @override
  void initState()
  {
    super.initState();
  }

    @override
  Widget build(BuildContext context) {
      return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children:[
            Text('Click the button below to open a file browser to select your Csv file, the file can be on the local device or in remote storage.'),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            Text('Once a file is selected the wizard will automatically progress to the next step.'),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            new RaisedButton(
              onPressed: () => _openFileExplorer(),
              child: new Text("Select a Csv file"),
            ),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            if(widget.fieldsData != null && widget.fieldsData.length > 0)
              Text('Found ${widget.fieldsData.length} rows to import')
          ]);
  }

  void _openFileExplorer() async {
    //API doesnt support custom files yet var filePath = await FilePicker.getFilePath(type: FileType.CUSTOM, fileExtension: 'csv');
    var filePath = await FilePicker.getFilePath(type: FileType.ANY);

    if(filePath.toLowerCase().endsWith(".csv"))
    {
      final input = new File(filePath).openRead();
      widget.fieldsData = await input.transform(utf8.decoder).transform(new CsvToListConverter(fieldDelimiter: widget.dataDelimiter, eol:widget.eol )).toList();

      if(widget.fieldsData.length > 0)
      {
        if(widget.ignoreFirstRow)
        {
          widget.fieldsData.removeAt(0);
        }
        widget.isNextButtonDisabled = false;
        widget.onStepCallback(widget.fieldsData, widget.isNextButtonDisabled, null);
        setState(() {});
      }
      else
      {
        widget.fieldsData = null;
        widget.isNextButtonDisabled = true;
        widget.onStepCallback(widget.fieldsData, widget.isNextButtonDisabled, new ImportAlert(AlertType.error, "Import error", "No rows found to import, please try changing the parameters for the import.", 2));
        setState(() {});
      }
    }
    else
    {
      widget.fieldsData = null;
      widget.isNextButtonDisabled = true;
      widget.onStepCallback(widget.fieldsData, widget.isNextButtonDisabled, new ImportAlert(AlertType.error, "Filetype error", "We can only import csv files at present, please ensure the file is a csv and the filename ends with .csv.", 3));
      setState(() {});
    }
  }
}