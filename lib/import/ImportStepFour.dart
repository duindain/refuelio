import 'package:flutter/material.dart';

typedef StepFourCallback = void Function(List<List<dynamic>> fieldsData, Map<String, int> columns, Map<String, String> columnsDisplay, bool isNextButtonDisabled);

class ImportStepFour extends StatefulWidget {
  List<List<dynamic>> fieldsData;
  Map<String, int> columns;
  Map<String, String> columnsDisplay;
  bool isNextButtonDisabled;
  String litresColumn;
  String kmsColumn;

  final StepFourCallback onStepCallback;

  ImportStepFour(this.fieldsData, this.columns, this.columnsDisplay, this.isNextButtonDisabled, this.litresColumn, this.kmsColumn, this.onStepCallback);

@override
  _ImportStepFourState createState() => new _ImportStepFourState();
}

class _ImportStepFourState extends State<ImportStepFour> {

  @override
  void initState()
  {
    super.initState();
    widget.columns = {
      'Servo': -1,
      'Cost': -1,
      widget.litresColumn: -1,
      widget.kmsColumn: -1,
      'Octane': -1,
      'Date': -1
    };
    var list = widget.columns.keys.toList();
    widget.columnsDisplay = {
      list[0]: list[0],
      list[1]: list[1],
      list[2]: list[2],
      list[3]: list[3],
      list[4]: list[4],
      list[5]: list[5]
    };
  }

    @override
  Widget build(BuildContext context) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('We need to identify which columns in the Csv match with the app, we\'ve displayed the first row of data, please drag and drop the values onto the relevant column names.'),
          Padding(padding: EdgeInsets.only(top: 20.0)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.max,
            children:[
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: widget.columns.keys.map((key) => _buildDragTarget(key, widget.columns[key])).toList()
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: widget.fieldsData[0].map((item) => _buildDraggableItem(item)).toList(),
              )]
          ),
        ]);
  }

  Widget _buildDraggableItem(item)
  {
    return Draggable<String>(
      data: item.toString(),
      feedback:Icon(Icons.toys, color: Colors.blueAccent,),
      //childWhenDragging:Icon(Icons.toys),
      child: Container(
        decoration:BoxDecoration(
          border: Border.all(width: 3.0),
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          color: Colors.white,
        ),
        alignment: Alignment.center,
        height:60,
        width:110,
        child:RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: item.toString(),
            style: TextStyle(color: Colors.black.withOpacity(0.6))
          )),
      ));
  }

  Widget _buildDragTarget(String name, int value) {
    return DragTarget(
      builder: (BuildContext context, List<String> incoming, List rejected) {
        return Container(
          decoration:BoxDecoration(
            border: Border.all(width: 3.0),
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            color: Colors.white,
          ),
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text: widget.columnsDisplay[name],
              style: TextStyle(color: Colors.black.withOpacity(0.6))
            )),
          alignment: Alignment.center,
          height: 70,
          width: 140,
        );
      },
      onWillAccept: (data) => true,
      onAccept: (data) {
        setState(() {
          //As soon as at least one column is assigned unlock the next page
          widget.isNextButtonDisabled = false;
          widget.columnsDisplay[name] = '$name\n$data';
          //print('Setting column $name to index of $data');
          widget.columns[name] = widget.fieldsData[0].indexWhere((a) => a.toString() == data.toString());
          widget.columnsDisplay.forEach((k, v) =>
          {
            if(k != name && v.contains(data))
            {
              widget.columnsDisplay[k] = v.substring(0, v.indexOf('\n')),
              widget.columns[name] = -1
            }
          });
          widget.onStepCallback(widget.fieldsData, widget.columns, widget.columnsDisplay, widget.isNextButtonDisabled);
        });
      },
      onLeave: (data) {},
    );
  }
}