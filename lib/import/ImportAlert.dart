import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import '../Utilities.dart';
import '../models/DatabaseService.dart';
import '../models/Refill.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:csv/csv.dart';
import 'dart:convert';

class ImportAlert {
  AlertType alertType;
  String title;
  String message;
  int goToStep;

  ImportAlert(
    this.alertType,
    this.title,
    this.message,
    this.goToStep
  );
 }